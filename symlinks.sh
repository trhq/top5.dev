#!/bin/bash
#Vars
projectRoot="/Users/paulbeynon/Sites/schoolwork/top5.dev";
pubRoot="/public";
public="$projectRoot$pubRoot";
ngRoot="/resources/assets/angular";
ng="$projectRoot$ngRoot";


#Files
files=();
files+=('css/style.css');
files+=('app');
files+=('templates');
files+=('bower_components');
files+=('bower_components/angular/angular.min.js');
files+=('bower_components/angular-ui-router/release/angular-ui-router.min.js');

rm $public"/css/style.css";
ln -s $ng"/css/style.css" $public"/css/";
#
echo "Linking $pubRoot/app";
rm $public"/app";
ln -s $ng"/app/" $public"/app";
#
echo "Linking $pubRoot/templates";
rm $public"/templates";
ln -s $ng"/templates" $public"/templates";
#
echo "Linking $pubRoot/templates";
rm $public"/js/angular.min.js";
# rm $public"js/angular-ui-router.min.js";
# rm $public"js/components";
# Remake SymLinks
# # Application Assets
# Angular Assets
# ln -s $ng"bower_components/angular/angular.min.js" $public"js/angular.min.js";
# ln -s $ng"bower_components/angular-ui-router/release/angular-ui-router.min.js" $public"js/angular-ui-router.min.js";
# Polymer Web Components
# ln -s $ng"bower_components" $public"js/components";
