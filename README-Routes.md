+--------+-----------+----------------------------------+----------------------+---------------------------------------------------------------+---------------+
| Domain | Method    | URI                              | Name                 | Action                                                        | Middleware    |
+--------+-----------+----------------------------------+----------------------+---------------------------------------------------------------+---------------+
|        | GET|HEAD  | /                                |                      | Closure                                                       | web           |
|        | GET|HEAD  | _debugbar/assets/javascript      | debugbar.assets.js   | Barryvdh\Debugbar\Controllers\AssetController@js              |               |
|        | GET|HEAD  | _debugbar/assets/stylesheets     | debugbar.assets.css  | Barryvdh\Debugbar\Controllers\AssetController@css             |               |
|        | GET|HEAD  | _debugbar/clockwork/{id}         | debugbar.clockwork   | Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork |               |
|        | GET|HEAD  | _debugbar/open                   | debugbar.openhandler | Barryvdh\Debugbar\Controllers\OpenHandlerController@handle    |               |
|        | GET|HEAD  | auth/login                       |                      | top5\Http\Controllers\Auth\AuthController@getLogin            | web,guest     |
|        | POST      | auth/login                       |                      | top5\Http\Controllers\Auth\AuthController@postLogin           | web,guest     |
|        | GET|HEAD  | auth/logout                      |                      | top5\Http\Controllers\Auth\AuthController@logout              | web           |
|        | GET|HEAD  | auth/register                    |                      | top5\Http\Controllers\Auth\AuthController@getRegister         | web,guest     |
|        | POST      | auth/register                    |                      | top5\Http\Controllers\Auth\AuthController@postRegister        | web,guest     |
|        | GET|HEAD  | login                            | login                | top5\Http\Controllers\Auth\AuthController@getLogin            | web,guest     |
|        | POST      | login                            | login                | top5\Http\Controllers\Auth\AuthController@postLogin           | web,guest     |
|        | GET|HEAD  | logout                           |                      | top5\Http\Controllers\Auth\AuthController@logout              | web           |
|        | GET|HEAD  | register                         | register             | top5\Http\Controllers\Auth\AuthController@getRegister         | web,guest     |
|        | POST      | register                         | register             | top5\Http\Controllers\Auth\AuthController@postRegister        | web,guest     |
|        | POST      | tags                             | tags.store           | top5\Http\Controllers\TagsController@store                    | web           |
|        | GET|HEAD  | tags                             | tags.index           | top5\Http\Controllers\TagsController@index                    | web           |
|        | GET|HEAD  | tags/create                      | tags.create          | top5\Http\Controllers\TagsController@create                   | web           |
|        | DELETE    | tags/{tags}                      | tags.destroy         | top5\Http\Controllers\TagsController@destroy                  | web           |
|        | PUT|PATCH | tags/{tags}                      | tags.update          | top5\Http\Controllers\TagsController@update                   | web           |
|        | GET|HEAD  | tags/{tags}                      | tags.show            | top5\Http\Controllers\TagsController@show                     | web           |
|        | GET|HEAD  | tags/{tags}/edit                 | tags.edit            | top5\Http\Controllers\TagsController@edit                     | web           |
|        | POST      | user                             | user.store           | top5\Http\Controllers\UserController@store                    | web,auth,user |
|        | GET|HEAD  | user                             | user.index           | top5\Http\Controllers\UserController@index                    | web,auth,user |
|        | GET|HEAD  | user/create                      | user.create          | top5\Http\Controllers\UserController@create                   | web,auth,user |
|        | PUT|PATCH | user/{user}                      | user.update          | top5\Http\Controllers\UserController@update                   | web,auth,user |
|        | GET|HEAD  | user/{user}                      | user.show            | top5\Http\Controllers\UserController@show                     | web,auth,user |
|        | DELETE    | user/{user}                      | user.destroy         | top5\Http\Controllers\UserController@destroy                  | web,auth,user |
|        | GET|HEAD  | user/{user}/edit                 | user.edit            | top5\Http\Controllers\UserController@edit                     | web,auth,user |
|        | POST      | user/{user}/images               | user.images.store    | top5\Http\Controllers\UserImageController@store               | web,auth,user |
|        | GET|HEAD  | user/{user}/images               | user.images.index    | top5\Http\Controllers\UserImageController@index               | web,auth,user |
|        | GET|HEAD  | user/{user}/images/create        | user.images.create   | top5\Http\Controllers\UserImageController@create              | web,auth,user |
|        | DELETE    | user/{user}/images/{images}      | user.images.destroy  | top5\Http\Controllers\UserImageController@destroy             | web,auth,user |
|        | PUT|PATCH | user/{user}/images/{images}      | user.images.update   | top5\Http\Controllers\UserImageController@update              | web,auth,user |
|        | GET|HEAD  | user/{user}/images/{images}      | user.images.show     | top5\Http\Controllers\UserImageController@show                | web,auth,user |
|        | GET|HEAD  | user/{user}/images/{images}/edit | user.images.edit     | top5\Http\Controllers\UserImageController@edit                | web,auth,user |
|        | GET|HEAD  | user/{user}/places               | user.places.index    | top5\Http\Controllers\UserPlaceController@index               | web,auth,user |
|        | POST      | user/{user}/places               | user.places.store    | top5\Http\Controllers\UserPlaceController@store               | web,auth,user |
|        | GET|HEAD  | user/{user}/places/create        | user.places.create   | top5\Http\Controllers\UserPlaceController@create              | web,auth,user |
|        | DELETE    | user/{user}/places/{places}      | user.places.destroy  | top5\Http\Controllers\UserPlaceController@destroy             | web,auth,user |
|        | GET|HEAD  | user/{user}/places/{places}      | user.places.show     | top5\Http\Controllers\UserPlaceController@show                | web,auth,user |
|        | PUT|PATCH | user/{user}/places/{places}      | user.places.update   | top5\Http\Controllers\UserPlaceController@update              | web,auth,user |
|        | GET|HEAD  | user/{user}/places/{places}/edit | user.places.edit     | top5\Http\Controllers\UserPlaceController@edit                | web,auth,user |
+--------+-----------+----------------------------------+----------------------+---------------------------------------------------------------+---------------+
