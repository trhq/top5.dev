<?php
namespace Database;
/**
 * This database seed adds test invoice data to the top5 application.
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use Faker\Factory as Factory;

use top5\User;
use top5\Model\Place;
use top5\Model\Price;
use top5\Model\Invoice\StatementRun;
use top5\Model\Invoice\Invoice;
use top5\Model\Invoice\LineItem;

trait DatabaseTraits
{
    protected $faker;
    protected $pricing;
    protected $users;
    protected $places;
    /**
     * Gets a random place name and address and
     * returns a place array.
     * @return array
     */
    public function randomPlace() {
        /* Get a random place name key and compile */
        $key = rand(0, count($this->placeNames) - 1);
        $place = $this->placeNames[$key];
        $place = array_merge($place, $this->randomAddress());
        $place["user_id"] = rand(9,39);
        /* Unset name and rebase */
        unset($this->placeNames[$key]);
        $this->placeNames = array_values($this->placeNames);
        
        return $place;
    }
    /**
     * An array of place names for random place creation.
     * @var array
     */
    protected $placeNames = array(
        array(
            "name" => "La Porchetta",
            "tags" => "Italian,Pizza",
        ),
        array(
            "name" => "Pepperonis",
            "tags" => "Takeaway,Pizza",
        ),
        array(
            "name" => "Dominos",
            "tags" => "Takeaway,Pizza",
        ),
        array(
            "name" => "Izakaya Den",
            "tags" => "Japanese,Asian,Restaurants",
        ),
        array(
            "name" => "Shuji Sushi",
            "tags" => "Japanese,Asian,Restaurants",
        ),
        array(
            "name" => "Yamagata",
            "tags" => "Japanese,Asian,Restaurants",
        ),
        array(
            "name" => "Ishiya Stonegrill",
            "tags" => "Japanese,Asian,Restaurants",
        ),
        array(
            "name" => "Sushi Hub",
            "tags" => "Japanese,Asian, Takeaway",
        ),
        array(
            "name" => "Bento Box",
            "tags" => "Japanese, Lunch, Takeaway",
        ),
        array(
            "name" => "Fancy Hanks",
            "tags" => "Lunch, Takeaway",
        ),
        array(
            "name" => "5 & Dime",
            "tags" => "Lunch, Bakery, Takeaway",
        ),
        array(
            "name" => "Boombap",
            "tags" => "Lunch, Bakery, Takeaway",
        ),
        array(
            "name" => "Schnitz",
            "tags" => "Lunch, Takeaway",
        ),
        array(
            "name" => "Chuckle Deli",
            "tags" => "Lunch, Takeaway, Bakery",
        ),
        array(
            "name" => "Arbory",
            "tags" => "Lunch, Counter Meals, Pubs & Bars",
        ),
        array(
            "name" => "Charles Dickens Hotel",
            "tags" => "Lunch, Counter Meals, Pubs & Bars",
        ),
        array(
            "name" => "Squires Loft",
            "tags" => "Lunch, Counter Meals, Pubs & Bars",
        ),
        array(
            "name" => "Spring Street Grocer",
            "tags" => "Lunch, Takeaway, Bakery",
        ),
        array(
            "name" => "The Oxford Scholar",
            "tags" => "Lunch, Counter Meals, Pubs & Bars",
        ),
        array(
            "name" => "Vietnom",
            "tags" => "Lunch, Bakery, Vietnamese",
        ),
        array(
            "name" => "Bar Americano",
            "tags" => "Lunch, Pubs & Bars",
        ),
        array(
            "name" => "Nobu",
            "tags" => "Restaurants,Asian, Japanese",
        ),
        array(
            "name" => "Kaneda",
            "tags" => "Restaurants,Asian, Japanese",
        ),
        array(
            "name" => "Yamato",
            "tags" => "Restaurants,Asian, Japanese",
        ),
        array(
            "name" => "Takumi",
            "tags" => "Restaurants,Asian, Japanese",
        ),
        array(
            "name" => "Kenzan",
            "tags" => "Restaurants,Asian, Japanese",
        ),
    );
    
    
    /**
     * And array of streets in Melbourne CBD and 
     * their safe number range for mapping.
     * @var array
     */
    protected $streets = array(
        array(
            "street"  => "Flinders Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 31, "max" => 522],
        ),
        array(
            "street"  => "Flinders Lane",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 31, "max" => 522],
        ),
        array(
            "street"  => "Collins Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 623],
        ),
        array(
            "street"  => "Little Collins Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 650],
        ),
        array(
            "street"  => "Bourke Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 10, "max" => 650],
        ),
        array(
            "street"  => "Little Bourke Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 650],
        ),
        array(
            "street"  => "Lonsdale Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 50, "max" => 650],
        ),
        array(
            "street"  => "Little Lonsdale Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 650],
        ),
        array(
            "street"  => "La Trobe Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 20, "max" => 500],
        ),
        array(
            "street"  => "Exhibition Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 300],
        ),
        array(
            "street"  => "Russell Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 350],
        ),
        array(
            "street"  => "Swanston Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 400],
        ),
        array(
            "street"  => "Elizabeth Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 400],
        ),
        array(
            "street"  => "Queen Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 300],
        ),
        array(
            "street"  => "William Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 100],
        ),
        array(
            "street"  => "King Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 1, "max" => 300],
        ),
        array(
            "street"  => "Spencer Street",
            "city"    => "Melbourne",
            "postcode"    => 3000,
            "numbers" => ["min" => 50, "max" => 300],
        ),
    );

    public function randomAddress() {
        $seed = $this->streets[rand(0, count($this->streets) - 1)];
        $addr["address"] = rand($seed["numbers"]["min"], $seed["numbers"]["max"]) . ' ' . $seed["street"];
        $addr["suburb"]   = $seed["city"];
        $addr["postcode"]   = $seed["postcode"];
        $addr["state"]   = "VIC";
        return $addr;
    }
    /**
     * A list to use for tag creation and random assignment.
     * @var array
     */
    protected $tags = array(
        'american',
        'asian',
        'australian',
        'bakery',
        'burgers',
        'cafe',
        'chinese',
        'counter meals',
        'coffee house',
        'degustation',
        'family',
        'fine dining',
        'fish & chips',
        'french',
        'gourmet',
        'greek',
        'indian',
        'italian',
        'japanese',
        'lunch',
        'organic',
        'pasta',
        'pizza',
        'pubs & bars',
        'restaurants',
        'seafood',
        'steak',
        'south american',
        'thai',
        'takeaway',
        'vegan',
        'vegetarian',
        'vietnamese',
    );
    /**
     * Returns a random Tag
     * @return string
     */
    public function randomTag() {
        return ucwords( $this->tags[ rand( 0, count($this->tags) -1 ) ] );
    }

    protected function faker() {
        $this->faker = Factory::create();
        $this->faker->seed(1000);
    }

    /**
     * Loops through all places and adds random 
     * values for clicks and displays.
     */
    protected function fudgePlaces() {
        /* Refresh users and places. */
        $this->users = User::all();
        $this->places = Place::all();

        /* Loop through each place and fudge its clicks and displays. */
        foreach($this->places as $place) {
            $place->clicks_after_last_bill   = rand(0, 350);
            $place->displays_after_last_bill = rand(0, 350);
            $place->save();
        }
    }

    /**
     * Compiles an runs statements with $dateString as the run date.
     * @param  string $dateString A datestring in the format d-M-Y (1-1-2016)
     */
    protected function runStatements($dateString) {
        $this->fudgePlaces();
    
        /* Create a new statement */
        $statement  = StatementRun::create([
            'run_at' => Carbon::parse($dateString)
        ]);

        /* Loop through users and compile their invoices. */
        foreach($this->users as $user) {
            if ( ! $user->is_admin) {
                if ($user->lastDisplays > $this->pricing->min('minimum_displays')) {
                    $invoice = new Invoice;
                    $invoice->user_id = $user->id;
                    $invoice->compile($user, $statement, $this->pricing);
                    $invoice->save();
                }
            }
        }
        $statement->resluggify();
        $statement->save();
    }

    /**
     * Gets all invoices in the system and pays them.
     */
    protected function payInvoices() {
        $invoices = Invoice::all();
        foreach ($invoices as $invoice) {
            $invoice->date_paid = $invoice->end_date->addDays(rand(1,30));
            $invoice->save();
        }
    }
}
