SELECT suburb, count(suburb) FROM `places`
GROUP BY suburb


/**
 * Gets all users and orders descending by place count.
 */
SELECT 	`users`.`username`,
		`users`.`firstname`, 
		`users`.`lastname`,
		CONCAT(`users`.`firstname`, ' ', `users`.`lastname`) as name,
		`users`.`slug`,
		`users`.`is_admin`,
		`users`.`created_at`,
		COUNT(`places`.id) as `placeCount`
FROM `users`
LEFT OUTER JOIN `places`
ON `users`.`id` = `places`.`user_id`
GROUP BY `users`.`id`
ORDER BY `placeCount` DESC
<?php 

$users = \DB::table('users')
->select('users.*')
->select('places.*')
->leftJoin('places', 'users.id', '=', 'places.user_id')
->groupBy('users.id')
->get();
// ->raw('count(places.*) as place_count')

$users = User::leftJoin('places', 'users.id', '=', 'places.user_id')
  ->selectRaw('users.*, count(places.id) as placeCount')
  ->groupBy('users.id')
  ->orderBy('placeCount', 'ASC')
  ->get();