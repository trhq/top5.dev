<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Database\DatabaseTraits;

use top5\Model\Tag;

class TagsTableSeeder extends Seeder
{
    use DatabaseTraits;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->tags as $tag) {
            $t = Tag::create([
                'name'  => ucwords($tag),
            ]);
            $t->resluggify();
            $t->save();
        }
        $tags = Tag::all();
        echo "Created ". $tags->count() . " Tags.\n";

    }
}
