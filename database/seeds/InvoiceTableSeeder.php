<?php
/**
 * This database seed adds test invoice data to the top5 application.
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use Database\DatabaseTraits;
use top5\User;
use top5\Model\Place;
use top5\Model\Price;
use top5\Model\Invoice\StatementRun;
use top5\Model\Invoice\Invoice;
use top5\Model\Invoice\LineItem;

class InvoiceTableSeeder extends Seeder
{
    use DatabaseTraits;

    /**
     * Gets the pricing, users and places from the database. 
     */
    public function __construct() {
        $this->pricing = Price::all();
        $this->users = User::all();
        $this->places = Place::all();
    }

    /**
     * Runs a year's worth of statements and invoices.
     */
    public function run()
    {
        $this->runStatements('2015-7-1');
        $this->runStatements('2015-8-1');
        $this->runStatements('2015-9-1');
        $this->runStatements('2015-10-1');
        $this->runStatements('2015-11-1');
        $this->runStatements('2015-12-1');
        $this->runStatements('2016-1-1');
        $this->runStatements('2016-2-1');
        $this->runStatements('2016-3-1');
        $this->runStatements('2016-4-1');
        /* Pay all existing invoices */
        $this->payInvoices();
        /* Add two more invoices, one overdue */
        $this->runStatements('2016-5-1');
        $this->runStatements('2016-6-1');

        /* And finally, fudge the data one last time to impress your friends */
        $this->fudgePlaces();

        $s = StatementRun::all();
        $i = Invoice::all();
        $l = LineItem::all();

        echo "Finished creating billing!\n";
        echo "Created " . $s->count() . " satement runs.\n";
        echo "Created " . $i->count() . " invoices.\n";
        echo "Created " . $l->count() . " line items.\n";

    }

}
