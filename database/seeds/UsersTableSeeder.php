<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon as Carbon;
use Faker\Factory as Factory;

use Database\DatabaseTraits;

use top5\User;
class UsersTableSeeder extends Seeder
{
    use DatabaseTraits;
    /**
     * Run the database seeds.
     * Adds a set of default users for development, 
     * all have password=password.
     *
     * @return void
     */
    public function run()
    {
    	$password = \Hash::make('password');
        $bob = "@bob.com";
        $now = Carbon::now();

        // Paul Beynon - Dev / Admin
        $user = User::create([
            "email" => 'paul.beynon@gmail.com',
            "password" => $password,
            "username" => "admin",
            "firstname" => "Paul",
            "lastname" => "Beynon",
            "description" => "Your friendly administrator.",
            "is_admin" => true,
            "created_at" => $now->subYears(2),
            "slug" => "admin"
        	]);
        $user->save();
        // Billy Bob - User
        $user = User::create([
            "email" => 'billy'.$bob,
            "password" => $password,
            "username" => "billyBob",
            "firstname" => "Billy",
        	"lastname" => "Bob",
            "description" => "Billy bob has the best burgers in town.",
            ]);
        $user->save();
        unset($user);

        // Jimmy Bob - User
        $user = User::create([
            "email" => 'jimmy'.$bob,
            "password" => $password,
        	"username" => "jimBob",
            "firstname" => "Jimmy",
            "lastname" => "Bob",
            "description" => "Epic Asian style!",
            ]);
        $user->save();
        unset($user);

        // Sammy Bob - User
        $user = User::create([
            "email" => 'sammy'.$bob,
            "password" => $password,
            "username" => "sammyBob",
            "firstname" => "Sammy",
            "lastname" => "Bob",
            "description" => "Jimmy Bob's pizzas are better than Billy Bob's burgers.",
            ]);
        $user->save();
        unset($user);

        // Robby Bob - User
        $user = User::create([
            "email" => 'robby'.$bob,
            "password" => $password,
            "username" => "robbyBob",
            "firstname" => "Robby",
            "lastname" => "Bob",
            "description" => "Robby Bob is the original baller.",
            ]);
        $user->save();
        unset($user);

        // Martha Bob - User
        $user = User::create([
            "email" => 'martha'.$bob,
            "password" => $password,
            "username" => "marthaBob",
            "firstname" => "Martha",
            "lastname" => "Bob",
            "description" => "Martha Bob is patient when it comes to the other Bobs.",
            ]);
        $user->save();
        unset($user);

        // Sally Bob - User
        $user = User::create([
            "email" => 'sally'.$bob,
            "password" => $password,
            "username" => "sallyBob",
            "firstname" => "Sally",
            "lastname" => "Bob",
            "description" => "Sally Bob is not down with the other Bobs.",
            ]);
        $user->save();
        unset($user);

        // Cletus Bob - User
        $user = User::create([
            "email" => 'cletus'.$bob,
            "password" => $password,
        	"username" => "cletusBob",
            "firstname" => "Cletus",
            "lastname" => "Bob",
            "description" => "Cletus specialises in original roadkill.",
            ]);
        $user->save();
        unset($user);

        /**
         * Create some fake user accounts
         */
        $this->faker();
        for ($i=0; $i<=30; $i++) {
            $user = User::create([
                'email' => $this->faker->safeEmail,
                'password' => $password,
                'username' => str_replace('.', '_', $this->faker->unique()->userName),
                'firstname' => $this->faker->firstName,
                'lastname' => $this->faker->lastName,
                'description' => $this->faker->paragraph(4)
            ]);
        }

        /**
         * Sluggify users then randomise join date.
         */
        $users = User::all();
        foreach($users as $user) {
            $user->resluggify();
            /* Only change creation date for non-admins */
            if( ! $user->is_admin ) {
                $now = Carbon::now();
                $user->created_at = $now->subDays(rand(0, 700));
                $user->save();
            }
        }
        echo "Finished creating " . $users->count() . " users...\n";
    }
}
