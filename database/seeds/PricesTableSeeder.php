<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Database\DatabaseTraits;

use top5\Model\Price;
class PricesTableSeeder extends Seeder
{
    use DatabaseTraits;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        unset($price);
        $price = Price::create([
            'minimum_displays' => 1500,
            'price_per_display' => 0.01,
        ]);
        unset($price);
        $price = Price::create([
            'minimum_displays' => 1000,
            'price_per_display' => 0.02,
        ]);
        $price = Price::create([
            'minimum_displays' => 500,
            'price_per_display' => 0.03,
        ]);
        unset($price);
        $price = Price::create([
            'minimum_displays' => 150,
            'price_per_display' => 0.04,
        ]);
        unset($price);
        $price = Price::create([
        	'minimum_displays' => 100,
        	'price_per_display' => 0.05,
        ]);
        unset($price);
    }
}
