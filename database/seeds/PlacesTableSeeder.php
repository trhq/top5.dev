<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Database\DatabaseTraits;

use top5\User;
use top5\Model\Place;
class PlacesTableSeeder extends Seeder
{
    use DatabaseTraits;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $state = "VIC";

        $url = "http://www.perkup.com.au";
        $email = "info@perkup.com.au";
        // Robby Bob - Perkup Burgers
        $place = Place::create([
            "user_id"     => 5,
            "name"        => "Perkup Burgers",
            "description" => "Perkup is a fun, energetic Cafe and Burger Bar for the best coffee, salads, burgers and fries in town. Come over and sink your teeth on our signature Perkup beef, chicken, kangaroo and gourmet veggie burgers!",
            "address"     => "610 Collins Street",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9620 2837",
            "website"     => $url,
            "email"       => $email,
        ]);
        $place->save();
        $place->tag('Burgers');
        $place->tag('Takeaway');
        $place->tag('Restaurants');
        unset($place);

    	// Robby Bob - Perkup Burgers
        $place = Place::create([
        	"user_id"     => 5,
        	"name"        => "Metro Burgers",
        	"description" => "Metro is a fun, energetic Cafe and Burger Bar for the best coffee, salads, burgers and fries in town. Come over and sink your teeth on our signature Perkup beef, chicken, kangaroo and gourmet veggie burgers!",
        	"address"     => "12 Degraves Street",
        	"suburb"      => "Melbourne",
        	"postcode"    => "3000",
        	"state"       => $state,
        	"phone"       => "03 9671 4069",
        	"website"     => $url,
        	"email"       => $email,
        ]);
        $place->save();
        $place->tag('Burgers');
        $place->tag('Takeaway');
        $place->tag('Restaurants');
        unset($place);

        //Billy Bob - Grill'd
        $place = Place::create([
            "user_id"     => 2,
            "name"        => "Grill'd, Degraves",
            "description" => "Awesome burgers with great flavour. Interesting chips, very yummy and filling.",
            "address"     => "15 Degraves Street",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9654 7666",
            "website"     => "https://www.grilld.com.au",
        ]);
        $place->save();
        $place->tag('Takeaway');
        $place->tag('Burgers');
        $place->tag('Restaurants');
        unset($place);

        //Billy Bob - Grill'd
        $place = Place::create([
            "user_id"     => 2,
            "name"        => "Grill'd, Melb Ctl",
            "description" => "Awesome burgers with great flavour. Interesting chips, very yummy and filling.",
            "address"     => "Level 3 - Melbourne Central",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9650 0085",
            "website"     => "https://www.grilld.com.au",
        ]);
        $place->save();
        $place->tag('Takeaway');
        $place->tag('Burgers');
        $place->tag('Restaurants');
        unset($place);
         //Billy Bob - Grill'd
        $place = Place::create([
            "user_id"     => 2,
            "name"        => "Grill'd, Little Bourke",
            "description" => "Awesome burgers with great flavour. Interesting chips, very yummy and filling.",
            "address"     => "369 Little Bourke Street",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9670 7100",
            "website"     => "https://www.grilld.com.au",
        ]);
        $place->save();
        $place->tag('Burgers');
        $place->tag('Restaurants');
        unset($place);

         //Billy Bob - Grill'd
        $place = Place::create([
            "user_id"     => 2,
            "name"        => "Grill'd, QV",
            "description" => "Awesome burgers with great flavour. Interesting chips, very yummy and filling.",
            "address"     => "222 Lonsdale Street",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9663 0399",
            "website"     => "https://www.grilld.com.au",
        ]);
        $place->save();
        $place->tag('Burgers');
        $place->tag('Restaurants');
        unset($place);

         //Billy Bob - Grill'd
        $place = Place::create([
            "user_id"     => 2,
            "name"        => "Grill'd, Sthrn Cross",
            "description" => "Awesome burgers with great flavour. Interesting chips, very yummy and filling.",
            "address"     => "Southern Cross Station",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9670 4100",
            "website"     => "https://www.grilld.com.au",
        ]);

        $place->save();
        $place->tag('Burgers');
        $place->tag('Restaurants');
        unset($place);
         //Billy Bob - Grill'd
        $place = Place::create([
            "user_id"     => 2,
            "name"        => "Grill'd, Southgate",
            "description" => "Awesome burgers with great flavour. Interesting chips, very yummy and filling.",
            "address"     => "Southgate",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9690 1033",
            "website"     => "https://www.grilld.com.au",
        ]);
        $place->save();
        $place->tag('Burgers');
        $place->tag('Restaurants');

        unset($place);
        //Sammy Bob - Crust
        $place = Place::create([
            "user_id"     => 4,
            "name"        => "Crust Pizza CBD",
            "description" => "Crust Pizza makes everyday moments more delightful. Crust Pizza makes any moment A Crust Above the ordinary.",
            "address"     => "377 Lonsdale Street",
            "suburb"      => "Melbourne",
            "postcode"    => "3000",
            "state"       => $state,
            "phone"       => "03 9602 1899",
            "website"     => "https://www.crust.com.au",
        ]);
        $place->save();

        $place->tag('Pizza');
        $place->tag('Takeaway');

         //Sammy Bob - Crust
        $place = Place::create([
            "user_id"     => 4,
            "name"        => "Crust Pizza Fitzroy",
            "description" => "Crust Pizza makes everyday moments more delightful. Crust Pizza makes any moment A Crust Above the ordinary.",
            "address"     => "350 Smith Street",
            "suburb"      => "Fitzroy",
            "postcode"    => "3064",
            "state"       => $state,
            "phone"       => "03 9095 0955",
            "website"     => "https://www.crust.com.au",
        ]);
        $place->save();
        $place->tag('Pizza');
        $place->tag('Takeaway');

         //Sammy Bob - Crust
        $place = Place::create([
        	"user_id"     => 4,
        	"name"        => "Crust Pizza Richmond",
            "description" => "Crust Pizza makes everyday moments more delightful. Crust Pizza makes any moment A Crust Above the ordinary.",
        	"address"     => "456 Church Street",
        	"suburb"      => "Richmond",
        	"postcode"    => "3121",
        	"state"       => "VIC",
        	"phone"       => "03 9429 3949",
        	"website"     => "https://www.crust.com.au",
        ]);
        $place->save();
        $place->tag('Takeaway');
        $place->tag('Pizza');

        // Jimmy Bob
        $place = Place::create([
            'user_id'     => 3,
            'name'        => 'Jim Wong Restaurant',
            'description' => 'Jim Wong make the 100% best lemon chicken in Footscray!',
            'phone'       => '03 9687 5971',
            'address'     => '259 Barkly Street',
            'suburb'      => 'Footscray',
            'postcode'    => 3011,
            'state'       => $state,
        ]);
        $place->save();
        $place->tag('Restaurants');
        $place->tag('Asian');
        $place->tag('Chinese');

        // Jimmy Bob
        $place = Place::create([
            'user_id'     => 3,
            'name'        => 'Poon\'s Chinese Restaurant',
            'description' => "Poon's fortune cookies have the best fortune. You eat at Poon's you go home lucky." ,
            'phone'       => '03 9687 4094',
            'address'     => '275 Barkly Street',
            'suburb'      => 'Footscray',
            'postcode'    => 3011,
            'state'       => $state,
        ]);
        $place->save();
        $place->tag('Restaurants');
        $place->tag('Asian');
        $place->tag('Chinese');

        $place = Place::create([
            'name'        => '8 Bit - Footscray',
            'user_id'     => 8,
            'address'     => '8 Droop Street',
            'suburb'      => 'Footscray',
            'postcode'    => 3011,
            'state'       => 'VIC',
            'description' => '8bit serves some of Melbourne’s finest hamburgers, milkshakes, fries and hotdogs. Hot dog options include the Fatal Fury, with jalapeno and cheddar sausage, cheese sauce and chilli beef.',
            'website'     => 'http://www.eat8bit.com.au/',
            'phone'       => '03 9687 8838',
            'email'       => 'hello@eat8bit.com.au',
        ]);
        $place->tag('Burgers');
        $place->tag('Takeaway');
        $place->tag('Restaurants');

        $place = Place::create([
            'name'        => '8 Bit - CBD',
            'user_id'     => 8,
            'address'     => '231 Swanston Street',
            'suburb'      => 'Melbourne',
            'postcode'    => 3000,
            'state'       => 'VIC',
            'description' => '8bit serves some of Melbourne’s finest hamburgers, milkshakes, fries and hotdogs. Hot dog options include the Fatal Fury, with jalapeno and cheddar sausage, cheese sauce and chilli beef.',
            'website'     => 'http://www.eat8bit.com.au/',
            'phone'       => '03 9687 8838',
            'email'       => 'hello@eat8bit.com.au',
        ]);
        $place->tag('Burgers');
        $place->tag('Takeaway');
        $place->tag('Restaurants');

        $place = Place::create([
            'name'        => 'DAsian Restaurant',
            'user_id'     => 9,
            'address'     => '68 Hopkins Street',
            'suburb'      => 'Footscray',
            'postcode'    => 3011,
            'state'       => 'VIC',
            'description' => 'Delicious Indo Chinese Food',
            'phone'       => '03 8354 8387',
        ]);
        $place->tag('Asian');
        $place->tag('Indian');
        $place->tag('Restaurants');

        $place = Place::create([
            'name'        => 'Dumplings & More',
            'user_id'     => 10,
            'address'     => '96 Hopkins Street',
            'suburb'      => 'Footscray ',
            'postcode'    => 3011,
            'state'       => 'VIC',
            'description' => 'Awesome food great prices and very friendly service.',
        ]);
        $place->tag('Asian');
        $place->tag('Chinese');
        $place->tag('Restaurants');

        $place = Place::create([
            'name'        => 'EBI Fine Foods',
            'user_id'     => 11,
            'address'     => '18 Essex Street',
            'suburb'      => 'Footscray ',
            'postcode'    => 3011,
            'state'       => 'VIC',
            'description' => 'Clever bento boxes, Japanese curry and fish ’n’ chips plated in a snug eatery with outdoor tables.',
            'phone'       => '03 9689 3300',
        ]);
        $place->tag('Asian');
        $place->tag('Japanese');
        $place->tag('Lunch');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Nhu Lan", 
            "address"     => "116 Hopkins Street", 
            "suburb"      => "Footscray",
            "state"       => "VIC",
            "postcode"    => 3011,
            "phone"       => "03 9689 7296",
            "description" => "Nhu Lan is a Footscray institution with a vast display of delicacies on offer. But without a doubt their Bahn Mi is what they are known for. The bread is phenomenal and one of the best in the area."
        ]);
        $place->tag('Asian');
        $place->tag('Vietnamese');
        $place->tag('Bakery');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Ba'get", 
            "address"     => "132 Russell Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 639 5035",
            "description" => "Funky counter-service spot for French baguettes filled with Vietnamese street food, with some seats."
        ]);
        $place->tag('Bakery');
        $place->tag('Vietnamese');
        $place->tag('Bakery');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Sunny Nguyen Bakery", 
            "address"     => "30 Irving Street", 
            "suburb"      => "Footscray",
            "state"       => "VIC",
            "postcode"    => 3011,
            "phone"       => "03 9687 7231",
            "description" => "Tasty and delicious rolls that are great to grab on the run. "
        ]);
        $place->tag('Asian');
        $place->tag('Vietnamese');
        $place->tag('Bakery');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "To's Cafe & Hot Bread", 
            "address"     => "122 Hopkins Street", 
            "suburb"      => "Footscray",
            "state"       => "VIC",
            "postcode"    => 3011,
            "phone"       => "03 9689 4849",
            "description" => " Experience the joy of crispy crackling in a crusty bread roll. Easily one of the best banh mi in Footscray."
        ]);
        $place->tag('Asian');
        $place->tag('Vietnamese');
        $place->tag('Bakery');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Om Vegetarian", 
            "address"     => "227 Collins Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9639 9412",
            "description" => "Unassuming pit stop for all-you-can-eat vegetarian Indian curry, saag and lassi, in an arcade."
        ]);
        $place->tag('Indian');
        $place->tag('Vegetarian');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Gopals Vegetarian Restaurant", 
            "address"     => "139 Swanston Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9650 1578",
            "description" => "Established, casual buffet restaurant serving European, Indian and Asian meat-free and vegan food."
        ]);
        $place->tag('Lunch');
        $place->tag('Vegetarian');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Bo de Trai", 
            "address"     => "94 Hopkins Street", 
            "suburb"      => "Footscray",
            "state"       => "VIC",
            "postcode"    => 3011,
            "phone"       => "03 9689 9909",
            "description" => "Bo De Trai is a cheap and cheerful vegetarian Vietnamese/Chinese restaurant in Footscray."
        ]);
        $place->tag('Vietnamese');
        $place->tag('Vegetarian');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Lentil as Anything", 
            "address"     => "223 Barkly Street", 
            "suburb"      => "Footscray",
            "state"       => "VIC",
            "postcode"    => 3011,
            "phone"       => "0401 810 890",
            "description" => "Bohemian vegetarian restaurant staffed by volunteers, offering casual, pay-as-you-feel dining."
        ]);
        $place->tag('Lunch');
        $place->tag('Vegetarian');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Bopha Devi Docklands", 
            "address"     => "27 Rakaia Way", 
            "suburb"      => "Docklands",
            "state"       => "VIC",
            "postcode"    => 3008,
            "phone"       => "03 9600 1887",
            "description" => "Stylish Cambodian restaurant offering traditional dishes, banquet menus and takeaway options."
        ]);
        $place->tag('Asian');
        $place->tag('Takeaway');
        $place->tag('Vegetarian');


        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Gaylord Indian Restaurant", 
            "address"     => "4 Tattersalls Lane", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9663 3980",
            "description" => "Large, established curry house with tandoori clay oven and mirrored ceiling, for familiar classics."
        ]);
        $place->tag('Indian');
        $place->tag('Restaurants');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Nirankar", 
            "address"     => "174 Queen Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9642 1995",
            "description" => "Warm and modern Indian/Nepalese restaurant with tablecloths and downlights, for familiar classics."
        ]);
        $place->tag('Indian');
        $place->tag('Restaurants');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Curry Vault", 
            "address"     => "20 Bank Place", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9600 0144",
            "description" => "Indian madras, masala and kormas, served in a homey dining room with simple decor and downlights."
        ]);
        $place->tag('Indian');
        $place->tag('Restaurants');

        $place = Place::create([
            "user_id"     => rand(5,29),
            "name"        => "Dhaka Restaurant", 
            "address"     => "145 Lonsdale Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9654 8109",
            "description" => "Traditional Indian dishes served in a small, modest setting with terracotta walls and a tiled floor."
        ]);
        $place->tag('Indian');
        $place->tag('Restaurants');

        // China Bar Restaurants
        $user_id = rand(5,29);
        $place = Place::create([
            "user_id"     => $user_id,
            "name"        => "China Bar - Russell Street", 
            "address"     => "235 Russell Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9639 1633",
            "description" => "Busy local stop for Chinese classics, like roast duck and dumplings, served until the late hours."
        ]);
        $place->tag('Asian');
        $place->tag('Chinese');
        $place->tag('Restaurants');

        $place = Place::create([
            "user_id"     => $user_id,
            "name"        => "China Bar Signature", 
            "address"     => "222 Exhibition Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9988 7778",
            "description" => "Busy restaurant with all-you-can-eat buffet, Chinese staples, sushi, sashimi, dim sum, and seafood."
        ]);
        $place->tag('Asian');
        $place->tag('Chinese');
        $place->tag('Restaurants');

        $place = Place::create([
            "user_id"     => $user_id,
            "name"        => "China Bar - Swanston Street", 
            "address"     => "257 Swanston Street", 
            "suburb"      => "Melbourne",
            "state"       => "VIC",
            "postcode"    => 3000,
            "phone"       => "03 9988 7778",
            "description" => "Quick service and best roast duck in melbourne at 3am."
        ]);
        $place->tag('Asian');
        $place->tag('Chinese');
        $place->tag('Restaurants');

        $place = Place::create([
            "user_id"     => $user_id,
            "name"        => "China Bar - Fitzroy", 
            "address"     => "325 Brunswick Street", 
            "suburb"      => "Fitzroy",
            "state"       => "VIC",
            "postcode"    => 3065,
            "phone"       => "03 9415 8118",
            "description" => "It's China Bar for hipsters!"
        ]);
        $place->tag('Asian');
        $place->tag('Chinese');
        $place->tag('Restaurants');

        $this->places = Place::all();
        /* Fudge clicks and displays */
        $this->fudgePlaces();
        foreach($this->places as $place) {
            /* Fudge rating, and all counts */
            $place->ratings = rand(55, 95);
            $place->all_clicks = rand(400, 5000);
            $place->all_displays = rand(400, 3000);

            /* Add slug and get google maps coordinates */
            $place->resluggify();
            $place->getCoordinates();
            $place->save();
        }
        /* Initialise faker and randomly assign dummy places. */
        $this->faker();
        $ll = count($this->placeNames);
        for($i=0; $i<$ll; $i++) {
            $seed = $this->randomPlace();
            $tags = $seed["tags"];
            unset($seed["tags"]);
            /** Create a randomly named place */
            $place = Place::create($seed);
            $place->description = $this->faker->paragraph;
            $place->tag( str_replace(', ', ',', $tags) );
            /* Fudge rating, and all counts */
            $place->ratings = rand(25, 65);
            $place->all_clicks = rand(400, 5000);
            $place->all_displays = rand(400, 3000);
            $place->resluggify();
            $place->getCoordinates();
            $place->save();
        }
        $this->places = Place::all();        
        echo "Finished creating " . $this->places->count() . " places!\n";
    }
}
