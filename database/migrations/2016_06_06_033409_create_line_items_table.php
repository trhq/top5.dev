<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->integer('display_total')->unsigned();
            $table->timestamps();
        });
        // Add foreign key to invoices.
        Schema::table('line_items', function (Blueprint $table) {
            $table->foreign('invoice_id')
                  ->references('id')->on('invoices')
                  ->onDelete('cascade');
            $table->foreign('place_id')
                  ->references('id')->on('places')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign keys on invoice line items
        Schema::table('line_items', function(Blueprint $table) {
         $table->dropForeign('line_items_invoice_id_foreign');
         $table->dropForeign('line_items_place_id_foreign');
        });
        Schema::drop('line_items');
    }
}
