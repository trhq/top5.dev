<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create places table.
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name', 50);
            $table->string('address', 100);
            $table->string('suburb', 45);
            $table->string('postcode', 4);
            $table->string('state', 3);
            $table->double('lat');
            $table->double('lng');
            $table->string('description', 255);
            $table->string('email', 255);
            $table->string('phone', 20);
            $table->string('website', 255);
            $table->integer('ratings');
            $table->double('price_per_display', 10, 2);
            $table->integer('all_clicks');
            $table->integer('clicks_after_last_bill');
            $table->integer('all_displays');
            $table->integer('displays_after_last_bill');
            $table->string('slug')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        // Add foreign key to places.
        Schema::table('places', function (Blueprint $table) {
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign keys on places
        Schema::table('places', function(Blueprint $table) {
         $table->dropForeign('places_user_id_foreign');
        });
        // Drop places table.
        Schema::drop('places');
    }
}