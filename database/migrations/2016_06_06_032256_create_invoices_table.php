<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('statement_run_id')->unsigned();
            $table->integer('user_id')->unsigned();
            // Invoice Terms
            $table->integer('term_days')->default(30);
            $table->integer('display_total');
            $table->double('display_price');
            $table->double('total_price');
            // Invoice Dates
            $table->date('start_date');
            $table->date('end_date');
            $table->date('date_paid')->nullable();
            // Record Keeping
            $table->string('slug')->nullable();
            $table->timestamps();
        });
        // Add foreign key to invoices.
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreign('statement_run_id')
                  ->references('id')->on('statement_runs')
                  ->onDelete('cascade');
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop foreign keys on invoices
        Schema::table('invoices', function(Blueprint $table) {
         $table->dropForeign('invoices_user_id_foreign');
         $table->dropForeign('invoices_statement_run_id_foreign');
        });
        Schema::drop('invoices');
    }
}
