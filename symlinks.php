#!/Applications/XAMPP/xamppfiles/bin/php
<?php
class SymLinker {
	public $links = [];
	protected $paths = array(
		"proj" => __DIR__,
		"ng"   => "/resources/assets/angular",
		"pub"  => "/public",
	);

	public function add($file, $link) {
		$this->links[] = array(
			"ng" => $file,
			"pub" => $link,
		);
		return $this;
	}
	public function doLinks() {
		foreach($this->links as $link) {
			$ng =  $this->ngPath($link["ng"]);
			$pub =  $this->pubPath($link["pub"]);
			if (file_exists($pub)) {
				unlink($pub);
			}
			echo "Linking: $ng\n";
			echo "     To: $pub\n";
			echo "========================================\n";
			shell_exec("ln -s $ng $pub");
		}
	}
	public function ngPath($file) {
		return $this->paths["proj"].$this->paths["ng"].$file;
	}
	public function pubPath($file) {
		return $this->paths["proj"].$this->paths["pub"].$file;
	}

}

$symLinks = new SymLinker;
$symLinks->add( "/css/style.css", "/css/style.css")
		 ->add( "/app", "/app")
		 ->add( "/templates", "/templates")
		 ->add( "/bower_components", "/js/components")
		 ->add( "/bower_components/angular/angular.min.js", "/js/angular.min.js")
		 ->add( "/bower_components/angular-ui-router/release/angular-ui-router.min.js", "/js/angular-ui-router.min.js")
		 ->add( "/bower_components/angular-google-maps/dist/angular-google-maps.min.js", "/js/angular-google-maps.min.js")
		 ->add( "/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js", "/js/angular-simple-logger.min.js")
		 ->add( "/bower_components/lodash/dist", "/js/lodash");

$symLinks->doLinks();
