requirejs.config({
	app: 'app',
	baseUrl: '/js',
	paths: {
		"jquery" : [
			'https://code.jquery.com/jquery-2.2.3.min',
			'dist/jquery.min'
		],
		"jqueryUi" : [
			'https://code.jquery.com/ui/1.11.4/jquery-ui.min',
			'dist/jquery-ui.min'
		],
		"bootstrap" : [
			// 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min',
			'dist/bootstrap'
		],
	},
	shim: {
		'bootstrap': { deps: ['jquery'] }
	}
});

require(['jquery', 'jqueryUi', 'bootstrap'], function($, $ui, bootstrap) {
	$(document).ready(function(){
		$(function() {
			function split( val ) {
				return val.split( /,\s*/ );
			}
			function extractLast( term ) {
				return split( term ).pop();
			}
			$( "#tags" )
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
					$( this ).autocomplete( "instance" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				source: function( request, response ) {
					$.getJSON( "http://top5.dev/tags?tagList=1&term="+extractLast(request.term), {
						term: extractLast( request.term )
					}, response );
				},
				search: function() {
					// custom minLength
					var term = extractLast( this.value );
					if ( term.length < 2 ) {
						return false;
					}
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( ", " );
					return false;
				}
			});
		}); // $(function)
	}); // document.ready
}); // Require
