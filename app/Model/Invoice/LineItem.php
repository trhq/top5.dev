<?php

namespace top5\Model\Invoice;

use Auth;

use Illuminate\Database\Eloquent\Model;

use top5\User;
use top5\Model\Place;
use top5\Model\UserTraits;
use top5\Model\Invoice\InvoiceTraits;

class LineItem extends Model
{
	use UserTraits;
	use InvoiceTraits;

	/**
	 * One to Many with Invoice.
	 */
	public function invoice() 
	{
		return $this->belongsTo('top5\Model\Invoice\Invoice');
	}
	/**
	 * Many to One with Place.
	 */
	public function place() 
	{
		return $this->belongsTo('top5\Model\Place');
	}
    /**
     * Returns the total price for this 
     * line item as a formatted string.
     * @return string   {$X,XXX.XX}
     */
    public function getLineTotalAttribute() {
        $total = $this->invoice->display_price * $this->display_total;
        return $this->formatDollar($total);
    }
    /**
     * Returns the unit price for this 
     * invoice as a formatted string.
     * @return string   {$X,XXX.XX}
     */
    public function getRateAttribute() {
        return $this->invoice->invoiceRate;
    }
}
