<?php

namespace top5\Model\Invoice;

use Auth;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use top5\User;
use top5\Model\Place;
use top5\Model\UserTraits;
use top5\Model\Invoice\LineItem;

use top5\Model\Invoice\InvoiceTraits;


class Invoice extends Model
{
	use SluggableTrait;
	use UserTraits;
    use InvoiceTraits;

    /**
     * Many to One with Statement Run.
     */
    public function statementRun() 
    {
        return $this->belongsTo('top5\Model\Invoice\StatementRun');
    }
	/**
	 * Many to One with user.
	 */
	public function user() 
	{
		return $this->belongsTo('top5\User');
	}

	/**
	 * One to Many with Invoice Line Items.
	 */
	public function lineItems() 
	{
		return $this->hasMany('top5\Model\Invoice\LineItem');
	}
    /**
     * Mass assignables.
     * @var array
     */
	protected $fillable = array(
		'display_total',
		'user_id',
		'term_days',
		'display_total',
		'display_price',
		'total_price',
		'start_date',
		'end_date',
		'date_paid',
		'slug',
	);

    /**
     * Build invoice slug from invoiceNumber attribute.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'invoiceNumber',
        'save_to' => 'slug',
        'on_update' => true,
    );

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'start_date', 'end_date', 'date_paid'];
    /**
     * Computes the invoice id and returns it 
     * as a preformatted invoice number.
     * @return string   {INV-XXXXXX}
     */
    public function getInvoiceNumberAttribute() {
        return 'INV-' . sprintf("%06d", $this->id);
    }
    /**
     * Returns the total price for this
     * invoice as a formatted string.
     * @return string   {$X,XXX.XX}
     */
    public function getInvoiceTotalAttribute() {
        return $this->formatDollar($this->total_price);
    }
    /**
     * Returns the unit price for this 
     * invoice as a formatted string.
     * @return string   {$X,XXX.XX}
     */
    public function getInvoiceRateAttribute() {
        return $this->formatDollar($this->display_price);
    }
    /**
     * Computes the due date for payment based 
     * on invoice date and term days.
     * @return string   {DD-Mmm-YYYY}.
     */
	public function getDueDateAttribute() {
		return $this->end_date->addDays($this->term_days)->format('d-M-Y');
	}
    /**
     * Checks to see if this invoice is overdue.
     * @return boolean true = overdue || false = not overdue
     */
    public function getOverdueAttribute() {
        return $this->end_date
                       ->addDays($this->term_days)
                       ->lt(Carbon::now());
    }
}
