<?php

namespace top5\Model\Invoice;

use Illuminate\Database\Eloquent\Model;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use top5\Model\Invoice\Invoice;

class StatementRun extends Model
{
    use SluggableTrait;
    
    /**
     * Makes user sluggable.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'statementNumber',
        'save_to' => 'slug',
        'on_update' => true,
    );

	protected $fillable = ['run_at'];
	protected $dates = [
		'run_at',
		'created_at',
		'updated_at',
	];
    public function invoices() {
    	return $this->hasMany('top5\Model\Invoice\Invoice');
    }

	public function getInvoiceCountAttribute() {
        return $this->invoices->count();
    }

	public function getRunTotalAttribute() {
        return '$'.number_format($this->invoices->sum('total_price'), 2);
    }
    /**
     * Computes the statement id and returns it 
     * as a preformatted statement number.
     * @return string   {Statement-XXXXXX}
     */
    public function getStatementNumberAttribute() {
        return 'Statement-' . sprintf("%06d", $this->id);
    }
}
