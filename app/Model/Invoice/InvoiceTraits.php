<?php

namespace top5\Model\Invoice;

use Auth;

use Carbon\Carbon;

use top5\User;
use top5\Model\Place;
use top5\Model\UserTraits;
use top5\Model\Invoice\LineItem;
use top5\Model\Invoice\StatementRun;

trait InvoiceTraits
{

    protected $user = null;
    protected $pricing = null;
    protected $statement = null;

    /**
     * Returns a number formatted to dollars and cents.
     * @param  double $price The price to format
     * @return string        Price formatted to dollars and cents.
     */
    public function formatDollar($price) {
        return '$'.number_format($price, 2);
    }


    /**
     * Compiles the line items and saves a new invoice
     * @return void
     */
    public function compile(User $user, $statement, $pricing) {
        /**
         * If this user is an administrator, don't compile 
         * an invoice. Otherwise, set the user id.
         */
        if ($user->is_admin) return;

        $this->pricing = $pricing;
        $this->statement = $statement;
        $this->user = $user;

        $this->user_id = $this->user->id;
        $this->statement_run_id = $this->statement->id;
        
        $this->setDates();
        $this->calculateDisplayPrice();
        
        $this->save();
        $this->createLineItems();

        $this->calculateTotals();
        $this->resluggify();
        $this->save();

        return;
    }

    /**
     * Sets the invoice start and end dates.
     */
    protected function setDates() {
        /**
         * Set today as the invoice end date.
         * @var Carbon/Carbon
         */
        $this->end_date = $this->statement->run_at;
        /**
         * Get the last invoice end date.
         */
        $lastInvoiceDate = Invoice::where('user_id', '=', $this->user_id)
                         ->orderBy('id', 'DESC')
                         ->take(1)->value('end_date');
        /**
         * Set the invoice start date based on last invoice.
         * 
         * No invoice   = A month before user creation date.
         * Last Invoice = The day after the last end date.
         * @var Carbon/Carbon
         */
        if( ! $lastInvoiceDate) {
            $lastInvoiceDate = $this->user->created_at->subYear();           
        }
        $this->start_date = $lastInvoiceDate->addDay();

        return $this;
    }

    /**
     * Calculates and sets the unit price for this invoice.
     */
    protected function calculateDisplayPrice() {
        /**
         * Check the pricing table and determine the per display 
         * rate for this billing period based on the total 
         * displays since the last bill.
         */
        foreach($this->pricing as $price) {
            // echo "Min Displays : $price->minimum = $price->price_per_display \n";
            if ($this->user->lastDisplays >= $price->minimum) {
                $this->display_price = $price->price_per_display;
                break;
            }
        }
        return $this;
    }

    protected function createLineItems() {
        /**
         * Save the invoice, then loop through each of 
         * the user's places to calculate line items.
         */
        foreach($this->user->places as $place) {
            
            /* Don't add if there are no views */
            if($place->displays_after_last_bill === 0)  continue; 
            
            /**
             * Create a new invoice line item.
             * @var top5/Invoice/LineItem
             */
            $line = new LineItem;
            $line->invoice_id = $this->id;
            $line->place_id = $place->id;
            $line->display_total = $place->displays_after_last_bill;
            $this->lineItems()->save($line);

            /**
             * Reset Place display and click counts.
             */
            $place->resetClicks()->resetDisplays()->save();
        }
        return;
    }
    /**
     * Calculates the total displays and 
     * @return [type] [description]
     */
    public function calculateTotals() {
        /**
         * Sum the line item display total, 
         * and calculate the total price.
         */
        $this->display_total = $this->lineItems->sum('display_total');
        $this->total_price = $this->display_price * $this->display_total;
    }   
}
