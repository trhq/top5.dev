<?php

namespace top5\Model;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
	/**
	 * Fillable fields.
	 * @var array
	 */
    protected $fillable = array(
    	'minimum_displays',
    	'price_per_display',
    );
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /** 
     * Creates a price mutator.
     */
    public function getPriceAttribute() {
        return number_format($this->price_per_display,2);
    }
    /** 
     * Creates a minimum click mutator.
     */
    public function getMinimumAttribute() {
        return $this->minimum_displays;
    }
}
