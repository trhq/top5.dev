<?php

namespace top5\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// Image Traits
use SahusoftCom\EloquentImageMutator\EloquentImageMutatorTrait;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

// Taggable Methods
use Cviebrock\EloquentTaggable\Taggable;

use top5\User;
use top5\Model\UserTraits;

class Image extends Model
{
    use EloquentImageMutatorTrait;
	use SluggableTrait;
	use Taggable;
	use SoftDeletes;
	use UserTraits;
	/**
	 * Defines a many to one relationship with user.
	 */
	public function user() 
	{
		return $this->belongsTo('top5\Model\User');
	}
    /**
     * An array of fields that store images
     *
     * @var array
     */
    protected $image_fields = ['picture'];

    protected $fillable = ['name', 'picture', 'description'];

    /**
     * Make Image sluggable.
     * @var array
     */
    protected $sluggable = array(
    	'build_from' => 'name',
    	'save_to' => 'slug'
    );
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];
	/**
	 * Validation rules for this model.
	 * @var array
	 */
	private $rules = array(
		'name' => 'required',
		'picture' => 'required',
		'description' => 'required',
	);
	/**
	 * Returns the validation rules for this model.
	 * @return array An array of validation rules.
	 */
	public function rules(){
	
		return $this->rules;
	}
}

