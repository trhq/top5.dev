<?php

namespace top5\Model;

use Illuminate\Database\Eloquent\Model;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use top5\Model\UserTraits;

class Tag extends \Cviebrock\EloquentTaggable\Models\Tag
{
    use SluggableTrait;

    protected $table = 'taggable_tags';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'slug'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Make place sluggable.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
        'on_update' => true,
    );

}
