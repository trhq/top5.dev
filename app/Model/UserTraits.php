<?php

namespace top5\Model;

use \Auth;
use Illuminate\Database\Eloquent\Model;

use top5\Http\Requests;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use top5\User;
use top5\Model\Place;

trait UserTraits
{
    /**
     * Returns the validation rules for this model.
     * @return array An array of validation rules.
     */
    public function rules(){
        return $this->rules;
    }
    public function isAdmin(){
    	$user = \Auth::user();
    	if ($user->isAdmin === true) {
    		return true;
    	}
    }
    /**
     * Formats and returns $num with $dec decimal places.
     * @param  number  $num The number to format.
     * @param  integer $dec The number of decimals.
     * @return string
     */
    public function formatNum($num, $dec=0) {
        return number_format($num, $dec);
    }

    /**
     * Checks to see if the user is an administrator. 
     * If so, it returns the requested user.
     * If not, it returns the authenticated user.
     * @param  string $slug     The slug of the user being requested.
     * @return top5/User
     */
    public function userAdminSlug($slug) {
        if (Auth::user()->is_admin === true) {
    		$user = User::findBySlug($slug);
    	} else {
            $user = Auth::user();
        }
    	return $user;
    }
    /**
     * Checks the user against the supplied slug and returns 
     * true if the user if found in the database
     * @param  top5/User    $user 
     * @param  string       $slug
     * @return mixed       
     */
    public function checkUserSlug(User $user, $slug='') {
        if ($slug !== $user->slug) {
            return [
                'found' => false , 
                'redirect' => redirect()->route('user.index')
                ->with('flash_message', "A user with slug <strong>$slug</strong> could not be found."),
            ];
        } else {
            return ["found" => true];
        }
    }

}
