<?php

namespace top5\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
// Taggable Methods
use Cviebrock\EloquentTaggable\Taggable;

use top5\User as User;
use top5\Model\UserTraits;

class Place extends Model
{
	use SluggableTrait;
	use Taggable;
	use SoftDeletes;
	use UserTraits;
	/**
	 * Defines a many to one relationship with user.
	 */
	public function user()
	{
		return $this->belongsTo('top5\User');
	}
	public function apiUser()
	{
	    return $this->belongsTo('top5\User', 'user_id', 'id')
	    			->select(['id', 'username', 'slug','description']);
	}
	/**
	 * Defines a one to many relationship with lineItems.
	 */
	public function lineItems()
	{
		return $this->belongsTo('top5\Model\Invoice\LineItem');
	}

	/**
	 * Fillable fields.
	 * @var array
	 */
    protected $fillable = array(
        'name',
        'description',
        'phone',
        'email',
        'website',
        'address',
        'suburb',
        'postcode',
        'state',
        'lat',
        'lng',
        'slug',
    );

    /**
     * Make place sluggable.
     * @var array
     */
    protected $sluggable = array(
    	'build_from' => 'name',
    	'save_to' => 'slug'
    );

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

	/**
	 * Validation rules for this model.
	 * @var array
	 */
	private $rules = array(
		'name' => 'required|max:50',
		'address' => 'required|max:100',
		'suburb' => 'required|max:45',
		'postcode' => 'required|min:4|max:4',
		'state' => 'required',
		'description'  => 'required|max:255',
		'email'  => 'max:255|email',
		'phone'  => 'max:20',
		'website'  => 'max:255',
		'lat'  => 'numeric',
		'lng'  => 'numeric',
	);

	/**
	 * Returns the validation rules for this model.
	 * @return array An array of validation rules.
	 */
	public function rules(){
		return $this->rules;
	}

	/**
	 * Validates passed model data against the model's validation rules.
	 * @param  Request $data User submitted data.
	 * @return [type]       [description]
	 */
	public function validate($data) {
		$v = \Validator::make($data, $this->rules);
		// Check for validation errors.
		if( $v->fails() )
		{
			$this->errors = $v->errors();
			return false;
		}
		// Otherwise, validation passes.
		return true;
	}

	/**
	 * Formats and returns the clicks since last bill
	 * @return string
	 */
	public function getClicksNowAttribute() {
		return $this->formatNum($this->clicks_after_last_bill);
	}
	/**
	 * Formats and returns all clicks for all time.
	 * @return string
	 */
	public function getClicksAllAttribute() {
		return $this->formatNum($this->all_clicks);
	}
	/**
	 * Formats and returns the displays since last bill
	 * @return string
	 */
	public function getDisplaysNowAttribute() {
		return $this->formatNum($this->displays_after_last_bill);
	}
	/**
	 * Formats and returns all displays for all time.
	 * @return string
	 */
	public function getDisplaysAllAttribute() {
		return $this->formatNum($this->all_displays);
	}

	/**
	 * Adds the current displays since last bill to the total
	 * displays and then resets to zero.
	 * @return  void
	 */
	public function resetDisplays() {
		$this->all_displays = $this->all_displays + $this->displays_after_last_bill;
		$this->displays_after_last_bill = 0;
		return $this;
	}

	/**
	 * Adds the current clicks since last bill to the total
	 * clicks and then resets to zero.
	 * @return  void
	 */
	public function resetClicks() {
		$this->all_clicks = $this->all_clicks + $this->clicks_after_last_bill;
		$this->clicks_after_last_bill = 0;
		return $this;
	}

	/**
	 * Makes a request to the google maps API and gets lat and long;
	 */
	public function getCoordinates() {
		/* Only fire on empty coordinates */
		// if ( ! $this->lat && ! $this->lng ) return;

		/* Google Maps Server API & Request String */
		$api = 'AIzaSyDa36W-jSJEMrfFiCVwXFrpwMCuwioh-Ec';
		$url = "https://maps.googleapis.com/maps/api/geocode/json?address=";

		/* Parse address to valid get query */
		$query = urlencode($this->address.$this->suburb.$this->state.'+');

		/* Request Coordinates from google. */
		$data = file_get_contents($url.$query.'Australia&key='.$api);
		$data = json_decode($data, true);

		/* Set lat and lng */
		if ( isset($data["results"][0]) ) {
			$data = $data["results"][0]["geometry"]["location"];
			$this->lat = $data["lat"];
			$this->lng = $data["lng"];
		}
	}
}
