<?php

namespace top5\Http\Middleware;

use Closure;
use Session;

/**
 * The AdminMiddleware handles requests to access the administrator's
 * area. If a user is not an administrator any requests should be 
 * redirected to their user area.
 *
 * This Middleware could be expanded to provide levels of permission
 * to administrators in the future. 
 */
class AdminMiddleware
{
    /**
     * Handle an incoming request to access administrative areas.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * These variables will be used to determine the 
         * appropriate actions for this request. 
         * 
         * @var $path    string        The requested path URI.
         * @var $auth    top5\User     The authenticated user.
         * @var $message string        Any flash message attached to the request.
         */
        $path = $request->route()->getPath();
        $auth = \Auth::user();
        $message = (Session::has('flash_message') ? Session::get('flash_message') : null);

        /**
         * If a user is not an administrator, 
         * redirect to their user page.
         */
        if ($auth->is_admin !== true) {
            return redirect()->route( 'user.show', ['slug' => $auth->slug])
                             ->with(['flash_message' => $message]);
        }

        // Otherwise, you got the touch. You got the power!!!
        return $next($request);
    }
}
