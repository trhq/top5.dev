<?php

namespace top5\Http\Middleware;

use Closure;
use Session;
use top5\User;

/**
 * The UserMiddleware adds handling requests in the user 
 * area. They ensure that users can only access their 
 * records, with the exception of administrators.
 */
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * These variables will be used to determine the 
         * appropriate actions for this request. 
         * 
         * @var $path    string        The requested path URI.
         * @var $auth    top5\User     The authenticated user.
         * @var $slug    string        The requested user slug.
         * @var $message string        Any flash message attached to the request.
         */
        $path = $request->route()->getPath();
        $auth = \Auth::user();
        $slug = $request->route()->user;
        $message = (Session::has('flash_message') ? Session::get('flash_message') : null);

        /**
         * Handle all non administrative requests first.
         */
        if ($auth->is_admin !== true) {

            /**
             * If no user slug is supplied, redirect to the user
             * to their user's page.
             */
            if ($slug === null ) {
                return redirect()->route( 'user.show', $auth->slug )
                                 ->with(['flash_message' => $message]);
            }

            /**
             * If the supplied slug does not match the user, redirect
             * the user to their user page.
             */
            if ($slug !== $auth->slug) {
                return redirect()->route( 'user.show', $auth->slug )
                                 ->with(['flash_message' => $message]);
            }
        
        } // End ! is_admin;

        /**
         * If an administrator has requested a user and one cannot be 
         * found, redirect them to the user index with a message.
         */
        if ($path !== 'user' && User::findBySlug($slug) === null) {
            return redirect()->route('user.index')
                             ->with(['flash_message' => 'That user could not be found.']);
        }

        // If you got to this point, everything is cool brah!
        return $next($request);
    }
}
