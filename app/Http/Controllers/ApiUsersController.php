<?php

namespace top5\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use top5\Http\Requests;

use top5\User;
use top5\Model\Place;
use top5\Model\Tag;

class ApiUsersController extends Controller
{
    protected $listFields = array(
    	'users.username',
    	'users.description',
    	'users.slug',
    );
    protected $users;
    /**
     * Returns a JSON array of users and their top 5 places.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* Get paginated users with apiUserListPlaces. */
        $this->users = User::has('apiUserListPlaces')
                    	->with('apiUserListPlaces')
						->select(['id', 'username', 'slug', 'description'])
                    	->get();

        /* Serialise and add to JSON response. */
        $this->serialise();
        $json = $this->users;

        return view('data.json-request')->with(['json' => $json]);
    }
    /**
     * Returns a JSON array of a single tag and their places.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug) {
        $this->users = User::where('slug', '=', $slug)
        				   ->has('apiUserSinglePlaces')
                    	   ->with('apiUserSinglePlaces')
                    	   ->select(['id', 'username', 'slug', 'description'])
                    	   ->get();
		$this->serialise(0);
        $json = $this->users;
        return view('data.json-request')->with(['json' => $json]);
    }
    /**
     * Loops through the users collection and serialises data
     * to standard json request.
     * @param  boolean $limit 	True:  Limit results to five places.
     *                         	False: Return all places / paginate.
     * @return void
     */
    protected function serialise($limit = true) {
        $users = $this->users;
        /* Strip relation fields and move apiUserListPlaces to places */
        foreach( $users as $u => $user ) {
			$user->name = '@'.$user->username;
			$user->placeCount = $user->places->count();
			$user->avgRating = round($user->places->average('ratings'));
			unset($user->places);
			$user->places = ( ($limit == true) ? $user->apiUserListPlaces->slice(0, 5) : $user->apiUserListPlaces);
			foreach($user->places as $p => $place) {
				unset($user->places[$p]->id, $user->places[$p]->user_id);
			}
			$users[$u] = $user;
			unset($users[$u]->apiUserListPlaces, $users[$u]->id);
        }
        $this->users = $users;
    }
}
