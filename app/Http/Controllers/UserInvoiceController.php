<?php

namespace top5\Http\Controllers;

use Illuminate\Http\Request;

use top5\Http\Requests;

use Carbon\Carbon;

use top5\User;
use top5\Model\Invoice\Invoice;
use top5\Model\Invoice\LineItem;

use top5\Http\Controllers\SortableTraits;

class UserInvoiceController extends Controller
{
    use SortableTraits;
    /**
     * Restricts access to authenticated users only.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'user']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug)
    {
        $this->setOptions($request);

        $user = User::findBySlug($slug);
        $user->invoices = Invoice::where('user_id', '=', $user->id)
                                 ->orderBy('id', 'DESC')
                                 ->paginate($this->results_per_page());

        return view('user.invoices.index')->with('user', $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user, $invoice)
    {
        $invoice = Invoice::findBySlug($invoice);
        return view('user.invoices.show')->with('invoice',$invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user, $invoice)
    {
        $user = User::findBySlug($user);
        $invoice = Invoice::findBySlug($invoice);
        $invoice->date_paid = Carbon::now();
        $invoice->save();
        return redirect()->route('user.invoices.index', $user->slug)
                         ->with('flash_message', "Thank you, <br> <strong>$invoice->invoiceNumber</strong> has been paid.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
