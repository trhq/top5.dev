<?php

namespace top5\Http\Controllers;

use Illuminate\Http\Request;

use top5\Http\Requests;

namespace top5\Http\Controllers;

use Auth;
use Session;

use Illuminate\Http\Request;

use top5\Http\Requests;
use top5\Http\Requests\UserRequest;

use top5\User;
use top5\Model\Place;
use top5\Model\Price;

use top5\Http\Controllers\SortableTraits;
use top5\Model\UserTraits;

class AdminController extends Controller
{
    use SortableTraits;
    use UserTraits;
    /**
     * Restricts access to authenticated users only.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display the admin index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Shows the admin panel for setting volume pricing on clicks.
     * @return \Illuminate\Http\Response
     */
    public function pricing() {
        $prices = Price::where('minimum_displays', '>', '0')->orderBy('minimum_displays')->get();
        return view('admin.price.edit')->with(['prices' => $prices]);
    }
    /**
     * Updates the pricing scheme with newly submitted data.
     * @param  Request $request 
     * @return redirect
     */
    public function pricingUpdate(Request $request) {
        $prices = $request->get('prices');

        foreach($prices as $id => $priceUpdate) {
            $price = Price::find($id);
            $price->price_per_display = $priceUpdate;
            $price->save();
        }

        return redirect()->route('admin.index')
                         ->with(['flash_message' => 'Pricing has been udpated.']);
    }
    /**
     * Administrator panel for viewing ALL places.
     * @return blade view
     */
    public function places(Request $request) {
        $this->setOptions($request);
        $session = $request->session()->all();
            $places = Place::orderBy($this->sortPlaceField(), $this->sortPlaceValue())
                           ->paginate($session['results_per_page']);

            return view('admin.place')->with(['places' => $places]);
    }
}
