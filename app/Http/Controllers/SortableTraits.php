<?php
namespace top5\Http\Controllers;

use Request;

trait SortableTraits
{
    /**
     * Sets the options for displaying results.
     * @param Request $request
     */
    protected function setOptions($request) {
            $options = $request->all();
            foreach ($options as $option => $value) {
                switch ($option) {
                    case 'username' :
                        $request->session()->put('sort.user.field', $option);
                        $request->session()->put('sort.user.value', $value);
                        break;
                    case 'placeCount' :
                        $request->session()->put('sort.user.field', $option);
                        $request->session()->put('sort.user.value', $value);
                        break;
                    case 'created_at' :
                        $request->session()->put('sort.user.field', 'created_at');
                        $request->session()->put('sort.user.value', $value);
                        break;
                    case 'user_places' :
                        $request->session()->put('sort.place.field', 'user_id');
                        $request->session()->put('sort.place.value', $value);
                        break;
                    case 'place_name' :
                        $request->session()->put('sort.place.field', 'name');
                        $request->session()->put('sort.place.value', $value);
                        break;
                    case 'results_per_page' :
                        $request->session()->put($option, $value);
                        break;                    
                }
            }
            $this->defaultOptions($request);
    }

    public function results_per_page() {
        return Request::session()->get('results_per_page');   
    }
    public function sortUserField() {
        return Request::session()->get('sort.user.field');
    }
    public function sortUserValue() {
        return Request::session()->get('sort.user.value');
    }
    
    public function sortPlaceField() {
        return Request::session()->get('sort.place.field');
    }
    public function sortPlaceValue() {
        return Request::session()->get('sort.place.value');
    }

    /**
     * Adds default options to the current session.
     */
    protected function defaultOptions($request) {
            if ( ! $request->session()->has('results_per_page')) {
                $request->session()->put('results_per_page', 10);
            }
            if ( ! $request->session()->has('sort.user.field')) {
                $request->session()->put('sort.user.field', 'username');
                $request->session()->put('sort.user.value', 'asc');
            }
            if ( ! $request->session()->has('sort.place.field')) {
                $request->session()->put('sort.place.field', 'name');
                $request->session()->put('sort.place.value', 'asc');
            }
    }

}
