<?php

namespace top5\Http\Controllers;

use Illuminate\Http\Request;

use top5\Http\Requests;
use top5\Http\Requests\UserPlaceRequest;

use top5\User as User;
use top5\Model\Place as Place;
use top5\Model\Invoice\LineItem as LineItem;

use top5\Http\Controllers\SortableTraits;
use top5\Model\UserTraits as UserTraits;

/**
 * This Controller handles interactions with an authenticated
 * user's user account.
 */
class UserPlaceController extends Controller
{
    use SortableTraits;
    use UserTraits;
    /**
     * Restricts access to authenticated users only.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'user']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug, Request $request)
    {
        $this->setOptions($request);
        $session = $request->session()->all();
        
        $user = User::findBySlug($slug);
        // dd($this->sortPlaceField(), $this->sortPlaceValue());
        $user->places = Place::where('user_id','=', $user->id)
                             ->orderBy($this->sortPlaceField(), $this->sortPlaceValue())
                             ->paginate($session["results_per_page"]);
        return view('user.places.index')
            ->with('user', $user)->with('places');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($userSlug)
    {   
        $user = User::findBySlug($userSlug);
        return view('user.places.create')->with(['user' => $user, 'place' => new Place]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPlaceRequest $request)
    {
        $user = \Auth::user();
        
        $place = new Place;
        $place->fill( $request->toArray() );
        $place->resluggify();
        
        $user->places()->save( $place );
        
        // Add tags if supplied.
        if ( strlen( $request->get('tags') ) > 0 ) {
            $place->tag( ucwords( $request->get( 'tags') ) );
        } 
        return redirect()->route('user.places.index', $user->slug)
                         ->with('flash_message', "<strong>$place->name</strong> was created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user, $place)
    {
        $place = Place::findBySlug($place);
        $place->user = User::find($place->user_id);
        $place->lineItems = LineItem::where('place_id', '=', $place->id)->orderBy('invoice_id', 'DESC')->get();
        return view('user.places.show')->with(['place' =>$place]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($userSlug, $placeSlug)
    {
        $user = User::findBySlug($userSlug);
        $place = Place::findBySlug($placeSlug);
        return view('user.places.edit')->with(['user' => $user, 'place' => $place]);
    }

    /**
     * Update the specified place in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserPlaceRequest $request, $userSlug, $placeSlug)
    {
        $place = Place::findBySlug($placeSlug);
        $place->fill( $request->toArray() );
        $place->resluggify();
        $place->save();
        
        /**
         * Handle Tags
         */
        if (strlen($request->get('tags')) < 1 && $place->tags->count()) {
            // Remove tags if none were supplied.
            $place->detag();
        } else {
            // Otherwise, check tags, determining which ones to add / remove.
            $tags = explode(',', ucwords( $request->get('tags') ) );
            $current_tags = array_column($place->tags->toArray(), 'name');
            sort($tags);
            sort($current_tags);
            // Add and removed changed tags.
            if ($tags != $current_tags) {
                $add  = array_diff($tags, $current_tags);
                $drop = array_diff($current_tags, $tags);
                $place->untag($drop);
                $place->tag($add);
            }
        } 
        
        return redirect()->route('user.places.index', $user->slug)
                         ->with('flash_message', "<strong>$place->name</strong> was updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $userSlug
     * @param  string  $placeSlug
     * @return \Illuminate\Http\Response
     */
    public function destroy($userSlug, $placeSlug)
    {
        $place = Place::findBySlug($placeSlug);
        $place->delete();
        return redirect()->route('user.places.index', $userSlug)
                         ->with('flash_message', "<strong>$place->name</strong> was deleted.");
        
    }
}
