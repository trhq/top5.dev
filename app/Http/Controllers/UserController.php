<?php

namespace top5\Http\Controllers;

use Auth;
use DB;
use Illuminate\Pagination\Paginator as Paginator;

use Illuminate\Http\Request;

use top5\Http\Requests;
use top5\Http\Requests\UserRequest;

use top5\User;
use top5\Model\Place;
use top5\Model\Invoice\Invoice;

use top5\Http\Controllers\SortableTraits;
use top5\Model\UserTraits;


class UserController extends Controller
{
    use SortableTraits;
    use UserTraits;
    /**
     * Restricts access to authenticated users only.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'user']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ( Auth::user()->is_admin !== true ) {
            return redirect()->route('user.show', ['slug' => $user->slug]);
        } else {
            $this->setOptions($request);
            $session = $request->session()->all();
            
            /**
             * Determine the appropriate query for sorting places.
             */
            if ($this->sortUserField() != 'placeCount') {
                $users = User::orderBy($this->sortUserField(), $this->sortUserValue())
                             ->paginate($this->results_per_page());
            } else {
                $users = User::leftJoin('places', 'users.id', '=', 'places.user_id')
                             ->selectRaw('users.*, count(places.id) as placeCount')
                             ->groupBy('users.id')
                             ->orderBy('placeCount', $this->sortUserValue())
                             ->paginate($session["results_per_page"]);
            }
            return view('user.admin')->with('users', $users);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        return redirect()->route('user.show', ['slug' => Auth::user()->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, UserRequest $request)
    {
        $user = User::findBySlug($slug);

        // Set request options and get session data
        $this->setOptions($request);
        $session = $request->session()->all();

        $user->places = Place::where('user_id', '=', $user->id)
                             ->paginate($session["results_per_page"],['*'],'places_page');

        $user->invoices = Invoice::where('user_id', '=', $user->id)
                                 ->orderBy('id', 'DESC')
                                 ->paginate($session["results_per_page"],['*'],'invoice_page');;

        return view('user.index')->with('user', $user);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $user = $this->userAdminSlug($slug);
        return view('user.edit')->with('user', $user);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $slug)
    {
        $user = User::findBySlug($slug);
        $user->fill($request->all());
        $user->save();
        
        return redirect()->route('user.show', $user->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $message = null;
        if( Auth::user()->is_admin) {
            $user = User::findBySlug($slug);
            $user->delete();
            $message = "<strong>$user->name</strong> has been deleted.";
        }
        return redirect()->route('user.index')
                ->with('flash_message', $message);
    }
}
