<?php

namespace top5\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use top5\Http\Requests;

use top5\Model\Place;
use top5\Model\Tag;

class ApiTagsController extends Controller
{
    protected $listFields = array(
      'name',
      'description',
      'slug',
      'address',
      'suburb',
      'postcode',
      'state',
      'lat',
      'lng',
      'email',
      'phone',
      'website',
      'ratings',
    );
    /**
     * Returns a JSON array of tags and their top 5 places.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $json =[];
        switch(true) {
            /**
             * Return a list of tag names if this is a request for a tag list.
             */
            case ($request->get('tagList')) :
                $query = DB::table('taggable_tags')
                          ->where('name', 'like', $request->get('term') . '%')
                          ->select('name')
                          ->orderBy('name', 'ASC');
                $tags = $query->get();
                foreach($tags as $tag) {
                    $json[] = strtolower($tag->name);
                }
                break;
            /**
             * Default to compiling an array of tags with their top five places.
             */
            default:
                $tags = Tag::all();
                foreach($tags as $key => $tag) {
                    $places = Place::withAllTags($tag->name)
                                   ->select($this->listFields)
                                   ->orderBy('ratings', 'desc')
                                   ->take(5)->get();
                    if ($places->count()) {
                      $json[] = array(
                        "name" => $tag->name,
                        "slug" => $tag->slug,
                        "places" => $places,
                      );
                    }
                }
                break;
        }

        return view('data.json-request')->with(['json' => $json]);
    }
    /**
     * Returns a JSON array of a single tag and their places.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug) {
      /*  Set the tag as results name. */
      $tag = Tag::select('*')
                ->where('slug', '=', $slug)
                ->first();
      // $json[0]["id"] = $tag->id;
      $json[0]["slug"] = $tag->slug;
      $json[0]["name"] = $tag->name;

      /* Paginate results for request */
      $json[0]["pages"] = Place::withAllTags($tag->name)
                             ->select($this->listFields)
                             ->orderBy('ratings', 'DESC')
                             ->paginate(5)->toArray();
      $json[0]["places"] = $json[0]["pages"]["data"];
      unset($json[0]["pages"]["data"]);
      return view('data.json-request')->with(['json' => $json]);
    }

}
