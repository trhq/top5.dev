<?php

namespace top5\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

use Carbon\Carbon;

use top5\Http\Requests;

use top5\User;
use top5\Model\Place;
use top5\Model\Price;
use top5\Model\Invoice\StatementRun;
use top5\Model\Invoice\Invoice;

use top5\Http\Controllers\SortableTraits;

class AdminInvoiceController extends Controller
{
    use SortableTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statements = StatementRun::orderBy('id', 'DESC')->get();
        return view('admin.invoices.index')
               ->with(['statements' => $statements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('is_admin', '=', false)->get();
        $pricing = Price::orderBy('minimum_displays','DESC')->get();
        return view('admin.invoices.create')
            ->with([
                'users' => $users, 
                'pricing' => $pricing
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Create a statment run and get the users 
         * and pricing table from the database.
         */
        $statement = StatementRun::create([
            "run_at" =>Carbon::now()
        ]);
        $users = User::where('is_admin', '=', false)->get();
        $pricing = Price::orderBy('minimum_displays','DESC')->get();
        
        /**
         * Check users and compile invoices for users 
         * who have displays since their last bill.
         */
        $pricing = Price::orderBy('minimum_displays','DESC')->get();
        foreach($users as $user) {
            if ($user->is_admin) continue;
            if ($user->lastDisplays > $pricing->min('minimum_displays')) {
                $invoice = new Invoice;
                $invoice->user_id = $user->id;
                $invoice->compile($user, $statement, $pricing);
                $invoice->save();
            }
        }

        // The invoice run is complete.
        return redirect()->route('admin.billing.create')
                         ->with(['flash_message' => 'Successfully created statement run ' . $statement->id . '.' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $statement = StatementRun::findBySlug($slug);
        $statement->invoices = Invoice::where('statement_run_id', '=', $statement->id)->paginate($this->results_per_page());
        return view('admin.invoices.show')->with('statement', $statement);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
