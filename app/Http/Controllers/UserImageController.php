<?php

namespace top5\Http\Controllers;

use Illuminate\Http\Request;

use top5\Http\Requests;
use top5\Http\Requests\UserImageRequest;

use top5\User as User;
use top5\Model\Image as Image;
use top5\Model\UserTraits as UserTraits;

class UserImageController extends Controller
{
    use UserTraits;
    /**
     * Restricts access to authenticated users only.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'user']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        return view('user.images.index')->with('user', $user)->with('images');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($userSlug)
    {
        $user = User::find(\Auth::id());
        $image = new Image;
        return view('user.images.create')->with(['user' => $user, 'image' => $image]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserImageRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
