<?php

namespace top5\Http\Requests;

use top5\Http\Requests\Request;

use top5\Model\Place;
use top5\User;

class UserPlaceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(\Auth::check()) return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    $place = new Place; 
    switch ($this->method() ) {
        case 'GET':
        case 'DELETE':
            return;
        case 'POST':
            return $place->rules();
        case 'PUT':
        case 'PATCH':
            return $place->rules();
        }
    }
}
