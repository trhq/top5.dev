<?php

namespace top5\Http\Requests;

use top5\Http\Requests\Request;

use top5\Model\Image;
use top5\User;

class UserImageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(\Auth::check()) return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    $image = new Image; 
    switch ($this->method() ) {
        case 'GET':
        case 'DELETE':
            return;
        case 'POST':
            return $image->rules();
        case 'PUT':
        case 'PATCH':
            return $image->rules();
        }
    }
}
