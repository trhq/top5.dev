<?php

namespace top5\Http\Requests;

use top5\Http\Requests\Request;
use top5\User;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(\Auth::check()) return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::findBySlug($this->segment(2));
        $rules = [
            'firstname'  => 'required|max:30',
            'lastname'  => 'required|max:30',
            'email' => 'required|max:255|email|unique:users,email,'. $user->id,
        ];
        switch ($this->method() ) {
            case 'GET':
                return[];
            case 'DELETE':
                return[];
            case 'POST':
                return $rules;
            case 'PUT':
                return $rules;
            case 'PATCH':
                return $rules;
        }
    }
}
