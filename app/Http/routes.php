<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    // return view('welcome');
    return view('public.index');
});


/**
 * Admin Controller
 * Handles requests from site administrators
 */
Route::resource('admin/billing', 'AdminInvoiceController');

Route::get('admin/places', [
    'as' => 'admin.places', 'uses' => 'AdminController@places'
]);
Route::get('admin/pricing', [
    'as' => 'admin.pricing', 'uses' => 'AdminController@pricing'
]);
Route::put('admin/pricing', [
    'as' => 'admin.pricing', 'uses' => 'AdminController@pricingUpdate'
]);
Route::resource('admin', 'AdminController');
Route::resource('admin/billing', 'AdminInvoiceController');

/**
 * User Controller
 * Handles requests to interact with a user's account.
 */
Route::resource('user', 'UserController');

/**
 * User Places Controller
 * Handles requests to interact with a user's places.
 */
Route::resource('user.places', 'UserPlaceController');


/**
 * User Places Controller
 * Handles requests to interact with a user's places.
 */
Route::resource('user.invoices', 'UserInvoiceController');

/**
 * User Images Controller
 * Handles requests to interact with a user's images.
 */
Route::resource('user.images', 'UserImageController');

/**
 * Api Controllers
 * Handles api requests for front end application.
 */
Route::resource('api/tags', 'ApiTagsController');
Route::resource('api/user', 'ApiUsersController');

/**
 * User Registration and Login
 * Handles new user creation and user login.
 */
Route::get(	'auth/register',
			'Auth\AuthController@getRegister');
Route::post('auth/register',
 			'Auth\AuthController@postRegister');
Route::get(	'auth/login',
			'Auth\AuthController@getLogin');
Route::post('auth/login',
 			'Auth\AuthController@postLogin');
Route::get('auth/logout',
		   'Auth\AuthController@logout');
/**
 * Logs a user out.
 */
Route::get('logout',
		   'Auth\AuthController@logout');
/**
 * Registration and Login Aliases
 */
Route::get('register', [
	'as' => 'register',
	'uses' => 'Auth\AuthController@getRegister'
]);
Route::post('register', [
	'as' => 'register',
	'uses' => 'Auth\AuthController@postRegister'
]);
Route::get('login', [
	'as' => 'login',
	'uses' => 'Auth\AuthController@getLogin'
]);
Route::post('login', [
	'as' => 'login',
	'uses' => 'Auth\AuthController@postLogin'
]);

