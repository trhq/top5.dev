<?php

namespace top5;

use Illuminate\Foundation\Auth\User as Authenticatable;

// Sluggable Methods
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use top5\Model\UserTraits;
use top5\Model\Price;

class User extends Authenticatable
{
    use SluggableTrait;
    use UserTraits;

    public $apiPlaceFields = array(
        'id',
        'user_id',
        'name',
        'slug',
        'description',
        'ratings',
        'address',
        'suburb',
        'postcode',
        'state',
        'lat',
        'lng',
    );

    /**
     * Defines a one to many relationship with Places
     * @return top5\Place
     */
    public function places() {
        return $this->hasMany('top5\Model\Place');
    }
    /**
     * Returns a user's places for a user list api request.
     * @return top5\Place
     */
    public function apiUserListPlaces() {
        return $this->hasMany('top5\Model\Place', 'user_id', 'id')
                    ->select($this->apiPlaceFields)
                    ->orderBy('ratings','desc');
    }
    /**
     * Returns a user's places for a single user api request.
     * @return top5\Place
     */
    public function apiUserSinglePlaces() {
        return $this->hasMany('top5\Model\Place', 'user_id', 'id')
                    ->select($this->apiPlaceFields)
                    ->orderBy('ratings','desc');
    }


    /**
     * Returns one to many places with public attributes.
     * @return top5\Place
     */


    /**
     * Defines a one to many relationship with images.
     * @return top5\Image
     */
    public function images() {
        return $this->hasMany('top5\Model\Image');
    }

    /**
     * Defines a one to many relationship with images.
     * @return top5\Image
     */
    public function invoices() {
        return $this->hasMany('top5\Model\Invoice\Invoice');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'username',
        'firstname',
        'lastname',
        'description',
        'slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
    ];

    /**
     * Makes user sluggable.
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'username',
        'save_to' => 'slug',
        'on_update' => true,
    );

    /**
     * Concatenates and returns the user's full name.
     * @return string
     */
    public function getFullNameAttribute() {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * Returns the user's full name with a link to their page.
     * @return string
     */
    public function getNameLinkAttribute() {
        return  $this->userLink( $this->getFullNameAttribute() );
    }

    /**
     * Returns the user name with a link to their page.
     * @return string
     */
    public function getUsernameLinkAttribute() {
        return $this->userLink( '&#64;' . $this->username );
    }
    /**
     * Calculates and returns the clicks since last bill.
     * @return  integer
     */
    public function getLastClicksAttribute() {
        return $this->places->sum('clicks_after_last_bill');
    }

    /**
     * Calculates and returns the total displays since last bill.
     * @return  integer
     */
    public function getLastDisplaysAttribute() {
        return $this->places->sum('displays_after_last_bill');
    }

    /**
     * Checks to see if the total displays for a user's places reaches
     * the minimum threshold for billing and returns total or 0.
     * @return integer
     */
    public function getBillingDisplaysAttribute() {
        $min_displays = Price::min('minimum_displays');
        if ($this->places->sum('displays_after_last_bill') >= $min_displays) {
            return $this->places->sum('displays_after_last_bill');
        } else {
            return 0;
        }
    }

    /**
     * Returns a link to this user's page.
     * @param  string $str Text to display for the link.
     * @return string
     */
    protected function userLink($str) {
        return '<a href="'.route('user.show', $this->slug).'">' . $str . "</a>";
    }

}
