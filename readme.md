# Top 5 Places

Top 5 Places is a fictional website for listing and rating places. 

### Users

* Public users can browse the Top 5 places by tag and view their locations and contact information. 

* Registered users can create and list and delete places.

### Admins

* Edit user profiles and places.

* Create statement runs which calculate a variable price based on the number of views per user in the current billing period and generate itemised invoices.

* Adjust bill pricing schemes. 

### Site

See below for tech specs.

* The application back-end is delivered using the Laravel framework. 

* The public front-end is delivered by an Angular 1.5 application.

* The admin / user area is delivered using Laravel Blade templates. 


##Technology

* Laravel
    - Provides full framework for RESTful web services.
    - Implements safeguards for preventing Cross Site Request Forgeries (CSRF).
    - Authorisation controller
        + Password and token hashing.
        + Limits login requests and sets a timeout if this is exceeded. 
    - Eloquent ORM 
        + Used to define object models and create their database entities and relationships.
        + Data migrations allow for database version control. Migrations include an up() and down() method so any implemented change can be rolled back.
        + Provides handlers for performing CRUD operations. 
    - Artisan command line interface
        + Includes tinker, a CLI which loads in the application namespace.
        + Can be used to create new models, migrations, controllers and more.
        + Provides information about the application configuration including defined routes, migration status, job status and more.
        + Can define and schedule custom commands to perform any application task. 
    - Blade Templates
        + Views can be defined using traditional PHP or with the Blade template engine. 
        + Blade is tailored to work with Laravel object models and features a number of methods for handling data and providing dynamic interactions. 
        

##TODO

* **Implement mail handling** 
    
    [See Issue](https://bitbucket.org/TeamRadHQ/top5.dev/issues/2/implement-mail-handling)


## License

[MIT license](http://opensource.org/licenses/MIT).