@extends('templates.partials.form')

@section('formPageTitle', "Update Account")

@section('formTitle', "$user->nameLink <small>Update Your Account</small>")

@section('form')

	{!! Form::model($user, array(
		'method' => 'PUT',
		'url' => route('user.update', $user->slug),
		'class' => 'form container'
	)) !!}

	@include('user.form-user', ["model" => $user])

	<p class="form-group col-xs-12">
		{!! Form::submit(
			'Update Your Account', [
			'class' => 'btn btn-block btn-primary'
		])!!}
	</p>

	{!! Form::close() !!}

@endsection
