@extends('templates.partials.form')

@section('formPageTitle', 'Edit ' . $image->name)

@section('formTitle', "$user->name <small>Edit <em>$image->name</em></small>")

@section('form')

	{!! Form::model($image, array(
		'method' => 'PUT',
		'url' => route('user.images.update', ['user' => $user->slug, 'image' => $image->slug]	),
		'class' => 'form container'
	)) !!}
	
	@include('user.images.form-place', ['user' => $user, 'image' => $image])

	<fieldset class="container">
			{!! Form::submit('Update ' . $image->name, ['class' => 'btn btn-lg btn-block btn-primary'])!!}
	</fieldset>
	{!! Form::close() !!}
@endsection

@section('javascript')

@endsection