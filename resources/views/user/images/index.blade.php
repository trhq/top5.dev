@extends('templates.main')

@section('pageTitle', 'Your Images')
@section('title', "$user->name <small>Your Images</small>")

@section('content')


<p>
	<a href="{{ route('user.images.create', $user->slug) }}" class="btn btn-success">
		<span class="glyphicon glyphicon-plus"></span>
		Add a new Image
	</a>
</p>

@endsection