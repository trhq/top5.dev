<fieldset class="container">
	<legend> Description </legend>
	@include('partials.input-name', ['model' => $image])
	@include('partials.input-description', ['model' => $image, 'helpText' => 'Enter a brief description of this image.'])
	@include('partials.input-picture', ['model' => $image])
</fieldset>
