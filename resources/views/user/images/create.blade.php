@extends('templates.partials.form')

@section('formPageTitle', 'Add a New Image')
@section('formTitle', "$user->name <small>Add a new image</small>")

@section('form')

	{!! Form::model('Image', array(
		'url' => route('user.images.store', $user->slug),
		'class' => 'form container'
	)) !!}
	
	@include('user.images.form-image', $user)

	<fieldset class="container">
			{!! Form::submit('Add New Image', ['class' => 'btn btn-lg btn-block btn-primary'])!!}
	</fieldset>
	{!! Form::close() !!}
@endsection