@extends('templates.main')

@section('pageTitle', "Your Account")
@section('title', "$user->fullName <small>Account</small>")

@section('content')


<section class="row container">
	<p class="col-sm-6">
		<a href="{{ route('user.edit', $user->slug) }}" class="btn btn-default btn-block">
			<span class="glyphicon glyphicon-pencil"></span>
			Edit Profile
		</a>
	</p>
	<p class="col-sm-6">
		<a href="#" class="btn btn-default btn-block">
			<span class="glyphicon glyphicon-lock"></span>
			Change Password
		</a>
	</p>
</section>

<section class="row places">
	@include('partials.table-user-places', ['places' => $user->places])
	<p> <a class="btn btn-block btn-primary btn-lg" href="{{route('user.places.index', $user->slug)}}">View All Places</a> </p>
</section>

@if( ! $user->is_admin)
<section class="row invoices">
	@include('partials.table-user-invoices', ['invoices' => $user->invoices])
	<a class="btn btn-block btn-primary btn-lg" href="{{route('user.invoices.index', $user->slug)}}">View All Invoices</a>
</section>
@endif

@endsection
