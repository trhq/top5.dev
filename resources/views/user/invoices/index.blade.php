@extends('templates.main')

@section('pageTitle', 'Your Invoices')
@section('title', "$user->nameLink <small>Invoices</small>")

@section('content')

@include('partials.table-user-invoices', ['invoices' => $user->invoices])

@endsection