@extends('templates.main')

@section('pageTitle', $invoice->invoiceNumber)
@section('title', $invoice->user->nameLink . " <small>$invoice->invoiceNumber</small>")

@section('content')

<section class="container">
<article class="panel panel-default">
	<header class="panel-heading container-fluid">
		<p class="lead col-xs-6" style="left: -1.5rem; padding-left:10px;">
			<strong>{{$invoice->start_date->format('d-M-Y')}} - {{$invoice->end_date->format('d-M-Y')}}</strong>
		</p>
		<p class="lead col-xs-6 text-right" style="left: 1.5rem; padding-right:10px;">
			<strong>@include('partials.invoice-due')</strong>
		</p>
	</header>
	
	<table class="table table-striped">
	<thead>
		<tr>
			<th class="col-xs-7">Place</th>
			<th class="col-xs-2 text-center">Views</th>
			<th class="col-xs-2 text-right">Total</th>
		</tr>
	</thead>
	<tbody>
	@foreach($invoice->lineItems as $line) 
		<tr>
			<th> 
				<a href="{{ route('user.places.show', [
					'user'=> $invoice->user->slug,
					'place'=> $line->place->slug
				])}}"> 
					{{$line->place->name}} 
				</a>
			</th>
			<td class="text-center"> 
				{{$line->display_total}} @ {{ $line->rate }}
			</td>
			<td class="text-right">
				{{ $line->lineTotal }}
			</td>
		</tr>
	@endforeach
	</tbody>
	</table>

	<footer class="panel-footer panel-primary container-fluid">
		<p class="lead">
			<span class="col-xs-8 text-left" style="left: -1.5rem; padding-left:5px;">
				{{ $invoice->lineItems->count() }} Places 
			</span>
			<span class="col-xs-2 text-right" style="">
				{{ $invoice->display_total }} Displays
			</span>
			<span class="col-xs-2 text-right" style="left: 1.5rem; padding-right:5px;">
				<strong>{{ $invoice->invoiceTotal }}</strong>
			</span>
			<span class="col-xs-1"></span>
		</p>
	@include('partials.invoice-pay', ['user' => $invoice->user,'invoice' => $invoice])	
	</footer>
	

</article>

</section>

@endsection