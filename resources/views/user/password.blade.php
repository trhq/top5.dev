@extends('templates.main')

@section('pageTitle', "Update Password")
@section('title', "$user->name <small>Update Your Password</small>")

@section('content')

@if (count($errors) > 0) 
<ul>
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
</ul>
@endif

{!! Form::model($user, array(
	'method' => 'POST',
	'url' => route('user.password', $user->slug),
	'class' => 'form container'
)) !!}
<p class="form-group col-xs-12">
	{!! Form::label('password', 'Current Password') !!}
	{!! Form::password('old', [
		"class" => "form-control",
		"placeholder" => "Enter your current password"
	]) !!}
</p>
<p class="form-group col-xs-12">
	{!! Form::label('new_password', 'New Password') !!}
	{!! Form::password('new', [
		"class" => "form-control",
		"placeholder" => "Enter your new password"
	]) !!}
</p>
<p class="form-group col-xs-12">
	{!! Form::password('new_confirmation', [
		"class" => "form-control",
		"placeholder" => "Confirm your new password"
	]) !!}
</p>
<p class="form-group col-xs-12">
	{!! Form::submit('Update Your Password', ['class' => 'btn btn-block btn-primary'])!!}
</p>

{!! Form::close() !!}

@endsection