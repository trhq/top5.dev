	<fieldset>
		<legend>User Account</legend>
		@include('partials.input-email', ['model' => $model])
		@include('partials.input-username', ['model' => $model])
	</fieldset>
	<fieldset>
		<legend>Profile</legend>
		@include('partials.input-name', ['model' => $model])
		@include('partials.input-description', [
			'model' => $model, 
			'helpText' => 'Add a brief description about the products and services your businesses offer. <br> <small>(255 characters max)</small>'
		])
	</fieldset>