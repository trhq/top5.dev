@extends('templates.main')

@section('pageTitle', 'Your Places')
@section('title', "$user->nameLink <small>Places</small>")

@section('content')


<p>
	<a href="{{ route('user.places.create', $user->slug) }}" class="btn btn-success">
		<span class="glyphicon glyphicon-plus"></span>
		Add a new Place
	</a>
</p>

@include('partials.table-user-places', ['places' => $user->places])

@endsection
