@extends('templates.partials.form')

@section('formPageTitle', 'Edit ' . $place->name)

@section('formTitle', "$user->nameLink <small><em>Edit $place->name </em></small>")

@section('form')

	{!! Form::model($place, array(
		'method' => 'PUT',
		'url' => route('user.places.update', ['user' => $user->slug, 'place' => $place->slug]	),
		'class' => 'form container'
	)) !!}

	@include('user.places.form-place', ['user' => $user, 'place' => $place])

	<fieldset class="container">
			{!! Form::submit('Update ' . $place->name, ['class' => 'btn btn-lg btn-block btn-primary'])!!}
	</fieldset>
	{!! Form::close() !!}
@endsection

