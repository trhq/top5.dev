<fieldset class="container">
	<legend> Description </legend>
	@include('partials.input-name', ['model' => $place])
	@include('partials.input-tags', ['model' => $place])
	@include('partials.input-description', ['model' => $place])
</fieldset>

<fieldset class="container">
	<legend> Contact </legend>
	@include('partials.input-phone', ['model' => $place])
	@include('partials.input-email', ['model' => $place])
	@include('partials.input-url', ['model' => $place])
</fieldset>

<fieldset class="container">
	<legend>Address</legend>
	@include('partials.input-address', ['model' => $place])
</fieldset>

<fieldset id="mapContents" class="container">
	<legend>Map reference</legend>
	<p class="form-group col-xs-6">
		{!! Form::text('lat', null, [
			"class" => "form-control",
			"placeholder" => "Latitude"
		]) !!}
	</p>
	<p class="form-group col-xs-6">
		{!! Form::text('lng', null, [
			"class" => "form-control",
			"placeholder" => "Longitude"
		]) !!}
	</p>
	<section id="map" class="col-xs-12" >

	</section>
</fieldset>
@include('scripts.mapDisplay')
