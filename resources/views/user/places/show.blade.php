@extends('templates.main')

@section('pageTitle', $place->name)
@section('title', $place->user->nameLink . " <small>$place->name</small>")

@section('content')


<section class="row container">

<article class="panel panel-primary place">
	<header class="panel-heading container-fluid">
		<h4 class="col-xs-6">{{$place->name}}</h4>
		<p class="col-xs-6">

			@include('templates.partials.button-delete', array(
				'model' => $place,
				'warning' => "This is a warning.",
				'class' => 'btn pull-right btn-danger col-sm-1',
				'route' => route('user.places.destroy', ['user' => $place->user->slug, 'place' => $place->slug]),
			))
			@include('templates.partials.button', array(
				'link'  => route('user.places.edit', ['user' => $place->user->slug, 'place' => $place->slug]),
				'icon'  => 'pencil',
				'class' => 'btn pull-right btn-default col-sm-1',
				'title' => "Edit $place->name",
				'text'  => 'Edit'
			))

		</p>
	</header>
<table class="table table-striped table-responsive">
<thead>
<tr>
	<td class="lead" colspan="2">
		<span class="col-xs-3 col-sm-3">
			<strong>Rating:</strong> {{$place->ratings}} <br>
		</span>
		<span class="col-sm-3 col-xs-3 text-right">
			<strong>Displays</strong> <br>
				{{$place->displaysNow}} <br>
				{{$place->displaysAll}} </span>
		<span class="col-sm-3 col-xs-3 text-right">
			<strong>Clicks</strong> <br>
				{{$place->clicksNow}} <br>
				{{$place->clicksAll}} </span>
		<span class="col-xs-1 text-right">
			<br>
			<small>Current</small><br>
			<small>All</small>
		</span>
	</td>
</tr>
</thead>
<tbody>
<tr>
	<th class="col-xs-4"> Description </th>
	<td class="col-sm-8"> {{$place->description}} </td>
</tr>
<tr>
	<th> Tags </th>
	<td>
		{{$place->tagList}}
	</td>
</tr>
@if($place->phone)
<tr>
	<th>
		Phone
	</th>
	<td>
		<a href="tel:{{$place->phone}}" title="Call {{$place->name}}">{{$place->phone}}</a>
	</td>
</tr>
@endif
@if($place->email)
<tr>
	<th>
		Email
	</th>
	<td>
		<a href="mailto:{{$place->email}}" title="Email {{$place->name}}">{{$place->email}}</a>
	</td>
</tr>
@endif
@if($place->website)
<tr>
	<th>
		Website
	</th>
	<td>
		<a href="{{$place->website}}" title="Visit {{$place->name}}'s Website">{{$place->website}}</a>
	</td>
</tr>
@endif
<tr>
	<td>
		<strong>Location</strong> <br>
		<address class="hidden-xs">
			{{$place->address}} <br>
			{{$place->suburb}} <br>
			{{$place->postcode}}, {{$place->state}}
		</address>
	</td>
	<td class="visible-xs">
		<address>
			{{$place->address}}, {{$place->suburb}}
		</address>
	</td>
	<td class="hidden-xs">
		<input type="hidden" name="lat" value="{{$place->lat}}">
		<input type="hidden" name="lng" value="{{$place->lng}}">
		<div id="map" style="border: 1px solid #666">
			@include('scripts.mapDisplay')
		</div>
	</td>
</tr>
</tbody>
</table>
</article>

@if($place->lineItems->count() > 0)
<article class="panel panel-primary line-items">
	<header class="panel-heading container-fluid">
		<h4 class="col-xs-6">View History</h4>
		<p class="col-xs-6 text-right">
{{-- 			@include('templates.partials.button', array(
				'link'  => route('user.places.edit', ['user' => $place->user->slug, 'place' => $place->slug]),
				'icon'  => 'pencil',
				'class' => 'pull-right btn-success',
				'title' => "Edit $place->name",
				'text'  => 'Edit'
			)) --}}
		</p>
	</header>
	<table class="table table-striped ">
	<thead>
	<tr class="bg-primary">
		<th class="text-left   col-sm-2 col-xs-3"> Invoice </th>
		<th class="text-center col-sm-2 hidden-xs"> From </th>
		<th class="text-center col-sm-2 col-xs-3">
			<span class="hidden-xs">To</span>
			<span class="visible-xs">Month</span>
		</th>
		<th class="text-center col-sm-2 col-xs-3"> Displays </th>
		<th class="text-center col-sm-1 col-xs-3"> Total </th>
		<th class="text-center col-sm-2 hidden-xs"> Actions </th>
	</tr>

	</thead>
	<tbody>
	<?php $total = 0; ?>
	@foreach($place->lineItems as $line)
	<?php
	$lineTotal = $line->display_total * $line->invoice->display_price;
	$total = $total + $lineTotal;
	?>
	<tr>
		<th class="text-left">
			<a href="{{route('user.invoices.show', ['user' => $place->user->slug, 'invice' => $line->invoice->slug])}}" title="View Invoice #{{$line->invoice->invoiceNumber}}">
				{{$line->invoice->invoiceNumber}}
			</a>
		</th>
		<td class="text-center hidden-xs"> {{$line->invoice->start_date->format('d-M-Y')}} </td>
		<td class="text-center">
			<span class="hidden-xs">{{$line->invoice->end_date->format('d-M-Y')}}</span>
			<span class="visible-xs">{{$line->invoice->end_date->format('M-Y')}}</span>
		</td>
		<td class="text-center"> {{$line->display_total}}  <span class="hidden-xs"> &#64; {{$line->invoice->invoiceRate}}</span> </td>
		<td class="text-center"> {{$line->formatDollar($lineTotal)}} </td>
		<td class="hidden-xs">
		@include('templates.partials.button', array(
			'link'  => route('user.invoices.show', ['user' => $place->user->slug, 'invice' => $line->invoice->slug]),
			'icon'  => 'search',
			'class' => 'btn-block btn btn-default',
			'title' => "View Invoice #" . $line->invoice->invoiceNumber,
			'text'  => 'View'
		))
		</td>
	</tr>
	@endforeach
	</tbody>
	<tfoot>
	<tr class="bg-primary">
		<th class="text-left   col-sm-2 col-xs-3">  </th>
		<th class="text-center col-sm-2 hidden-xs"> </th>
		<th class="text-center col-sm-2 col-xs-3 lead"> Total </th>
		<th class="text-center col-sm-1 col-xs-3 lead"> {{$place->all_displays}} </th>
		<th class="text-center col-sm-2 col-xs-3 lead"> {{$line->formatDollar($total)}}  </th>
		<th class="text-center col-sm-2 hidden-xs"> </th>
	</tr>
	</tfoot>
	</table>
</article>
@endif
</section>

@endsection
