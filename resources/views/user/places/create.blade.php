@extends('templates.partials.form')

@section('formPageTitle', 'Add a New place')
@section('formTitle', "$user->nameLink <small>Add a new place</small>")

@section('form')

	{!! Form::model('Place', array(
		'url' => route('user.places.store', $user->slug),
		'class' => 'form container'
	)) !!}

	@include('user.places.form-place', $user)

	<fieldset class="container">
			{!! Form::submit('Add New Place', ['class' => 'btn btn-lg btn-block btn-primary'])!!}
	</fieldset>
	{!! Form::close() !!}
@endsection
