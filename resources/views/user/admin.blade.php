@extends('templates.main')

@section('pageTitle', "Manage Users - Top 5")
@section('title', "Manage Users	 <small>Top 5</small>")

@section('content')


<section class="container">

<article class="panel panel-primary">
	<header class="panel-heading container-fluid">
		<h4 class="col-xs-6">Title</h4>
		<p class="col-xs-6 text-right">
			@include('partials.input-results_per_page')
		</p>
	</header>

	<table class="table table-striped table-hover container">
	<thead>
		<tr>
			<th class="col-sm-3">
				@include('partials.link-sorting', array(
					'modelName' => 'user',
					'label' => 'User',
					'field' => 'username',
					'title' => 'Sort by User Name',
					'route' => Request::route()->getName(),
				))
			</th>
			<th class="col-sm-3 hidden-xs hidden-sm">
				@include('partials.link-sorting', array(
					'modelName' => 'user',
					'label' => 'Join Date',
					'field' => 'created_at',
					'title' => 'Sort by Join Date',
					'route' => Request::route()->getName(),
				))

			</th>
			<th class="hidden-xs text-center">
				@include('partials.link-sorting', array(
					'modelName' => 'user',
					'label' => 'Places',
					'field' => 'placeCount',
					'title' => 'Sort by Place Count',
					'route' => Request::route()->getName(),
				))
			</th>
			<th class="col-md-3 col-sm-4 col-xs-5 text-center">
				Actions
			</th>
		</tr>
	</thead>
	<tbody>
@foreach($users as $user) 
		<tr>
			<th> 
				{!!$user->usernameLink!!}
				<br class="visible-xs">
				<span class="hidden-xs">/</span>
				{!!$user->nameLink!!} 
		 	</th>
			<td class="hidden-xs hidden-sm"> 
				{!! $user->created_at->format('d M Y') !!}
			</td>
			<td class="hidden-xs text-center"> 
				{{$user->places->count()}}
			</td>
			<td class="text-left">
				@include('templates.partials.button', array(
					'link'  => route('user.show', ['slug' => $user->slug]),
					'icon'  => 'search',
					'title' => "View $user->name",
					'text'  => 'View'
				))
				@include('templates.partials.button', array(
					'link'  => route('user.edit', ['slug' => $user->slug]),
					'icon'  => 'pencil',
					'title' => "Edit $user->name",
					'text'  => 'Edit'
				))

				@if(! $user->is_admin)
				@include('templates.partials.button-delete', [
					'warning' => "This will permanently delete all of $user->name's profile and billing data, and {$user->places->count()} places.",
					'model' => $user,
					'route' => route('user.destroy', ['user' => $user->slug]),

				])
				@endif
			</td>
		</tr>
@endforeach
	</tbody>
	<tfoot>
		<td colspan="4" class="text-center">{{ $users->links() }}</td>
	</tfoot>
	{{-- @endforeach --}}
</table>
</article>

</section>


@endsection