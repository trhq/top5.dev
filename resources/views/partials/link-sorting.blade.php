<?php
$url = route(Request::route()->getName(), Route::getCurrentRoute()->parameters());
$sortField = Request::session()->get("sort.$modelName.field");
$sortField = ('name' == $sortField && $modelName == 'place' ? "place_$sortField" : $sortField);
?>

@if( $field == $sortField && Request::session()->get("sort.$modelName.value") == 'asc' )
	<a href="{!!url("$url?$field=desc")!!}" 
	   title="{!!str_replace('Sort', 'Sort Descending', $title)!!}">
	   {{$label}} 
	</a>
	<span class="dropup"><span class="caret"></span></span>
@else
	<a href="{!!url("$url?$field=asc")!!}" title="{!!str_replace('Sort', 'Sort Ascending', $title)!!}">
		{{$label}}  
	</a>
	<span class="caret"></span>
@endif