<?php
$modelName = str_replace('top5\\', '', get_class($model));
switch ($modelName) {
	case 'Place': 
		$icon = 'briefcase'; break;
	case 'User':
		$icon = 'user'; break;
	default:
		$icon = 'pencil';
}
?>

@if ($modelName != 'User')
	@include('templates.partials.input', [
		'field' => 'name',
		'icon' => $icon,
		'placeholder'=>'Enter a name...', 
		'helpText' => 'The Name field is required',
		'model' => $model
	])
@else 
	@include('templates.partials.input', [
		'field' => 'name',
		'icon' => $icon,
		'type' => 'group',
		'helpText' => 'Provide your first and last names. <br> <small>(30 characters each)</small>',
		'model' => $model,
		'fields' => array(
			Form::text(
				'firstname', 
				null, 
				[
					"class" => "form-control",
					"placeholder" => "First Name"
			]),
			Form::text(
				'lastname', 
				null, 
				[
					"class" => "form-control", 
					"placeholder" => "Last Name"
			]),
		),
	])
@endif