<label for="results_per_page">Results:</label>
<span class="col-lg-3 col-md-3 col-sm-4 col-xs-6 pull-right">
	<select name="results_per_page" class="form-control">
		@foreach([5,10,15,20] as $option)
			<option
				@if(Session::get('results_per_page') == $option)
					selected
				@endif 
			value="{{$option}}" >{{$option}}</option>
		@endforeach
	</select>
	@include('scripts.results_per_page')
</span>