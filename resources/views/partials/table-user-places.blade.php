<?php $route = route(Request::route()->getName(), Route::getCurrentRoute()->parameters());?>
<article class="panel panel-primary">
	<header class="panel-heading container-fluid">
		<h4 class="col-xs-6">Places</h4>
		<p class="col-xs-6 text-right">
			@include('partials.input-results_per_page')
		</p>
	</header>

{{-- Show places if there are any --}}
@if($places->count())
	<table class="table table-striped">
	<thead>
		<tr>
		@if(Request::url() === route('admin.places'))
		<th>
			@include('partials.link-sorting', array(
				'modelName' => 'place',
				'label' => 'User',
				'field' => 'user_id',
				'title' => 'Sort by Username',
				'route' => route(Request::route()->getName(), Route::getCurrentRoute()->parameters()),
			))
		</th>
		@endif

		<th>
			@include('partials.link-sorting', array(
				'modelName' => 'place',
				'label' => 'Name',
				'field' => 'place_name',
				'title' => 'Sort by Place Name',
				'route' => Request::route()->getName(),
			))
		</th>
		@if(Request::url() !== route('admin.places'))
			<th class="hidden-xs hidden-sm">City</th>
		@endif
			<th class="hidden-xs">Clicks</th>
			<th class="hidden-xs">Displays</th>
			<th class="text-center">Actions</th>
		</tr>
	</thead>
	<tbody>
	@foreach($places as $place) 
		<tr>
			@if(Request::url() === route('admin.places'))
			<th>
				<a href="{{route('user.show', ['user' => $place->user->slug])}}">
					&#64;{!! $place->user->username !!}
				</a>
			</th>
			@endif
			<th> 
			<a href="{{route('user.places.show', ['user' => $place->user->slug, 'place' => $place->slug])}}">
				{{$place->name}} 
			</th>
			@if(Request::url() !== route('admin.places'))
				<td class="hidden-xs hidden-sm"> 
					 {{$place->suburb}}
				</td>
			@endif
			<td class="hidden-xs"> 
				 {{$place->displays_after_last_bill}}
				 <small><em>({{$place->all_displays}})</em></small>
			</td>
			<td class="hidden-xs"> 
				 {{$place->clicks_after_last_bill}}
				 <small><em>({{$place->all_clicks}})</em></small>
			</td>
			<td>
				@include('templates.partials.button', array(
					'link'  => route('user.places.show', ['user'  => $place->user->slug, 'place' => $place->slug]),
					'icon'  => 'search',
					'title' => "View $place->name",
					'text'  => 'View'
				))
				@include('templates.partials.button', array(
					'link'  => route('user.places.edit', ['user'  => $place->user->slug, 'place' => $place->slug]),
					'icon'  => 'pencil',
					'title' => "Edit $place->name",
					'text'  => 'Edit'
				))
				@include('partials.button-delete')
			</td>
		</tr>
	@endforeach
	</tbody>
	</table>
	</ul>
{{-- Show blank placeholder --}}
@else 
	<div class="panel-body">
		<p class="lead"> You haven't added any places yet. </p>
		<p class="lead"> <a href="{!! route('user.places.create', Auth::user()->slug)!!}" class="link">Add your first place...</a> </p>
	</div>
@endif
{{-- Only show pagintation if there are more places --}}
@if ($places->links())
	<footer class="panel-footer panel-primary container-fluid text-center">
		{{ $places->links() }}
	</footer>
@endif
</article>