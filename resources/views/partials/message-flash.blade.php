{{-- Checks to see if a session has been flashed --}}
@if(\Session::has('flash_message'))
{{-- Displays the flash message in a bootstrap alert --}}
  <div class="message container" style="position:absolute; top: 70px;">
    <div class="alert alert-success" style="margin:0 auto; max-width: 400px">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <em> {!! session('flash_message') !!}</em>
    </div>
  </div>
{{-- Hides the message and removes it from the document --}}
  <script>
  @include('partials.openJS')
    setTimeout(function() { jQuery(".alert").slideUp(1500); }, 3500);
    setTimeout(function() { jQuery(".message").remove(); }, 5000);
  @include('partials.closeJS')
  </script>
@endif