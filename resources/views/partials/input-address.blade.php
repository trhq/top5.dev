<?php 
$fields = [
	Form::text(
		'address', 
		null, 
		[
			"class" => "form-control",
			"placeholder" => "Street"
	]),
	Form::text(
		'suburb', 
		null, 
		[
			"class" => "form-control", 
			"placeholder" => "Suburb"
	]),
	Form::text(
		'postcode', 
		null, 
		[
			"class" => "form-control", 
			"placeholder" => "Postcode"
	]),
	Form::select(
		'state', 
		[
			'ACT' => 'Australian Capital Territory',
			'NT' => 'Northern Territory',
			'NSW' => 'New South Wales',
			'QLD' => 'Queensland',
			'VIC' => 'Victoria',
			'WA' => 'Western Australia',
		], 
		'VIC', 
		[
			'placeholder' => 'Select a State...',
			'class' => 'form-control'
	]),
];
?>
@include('templates.partials.input', [
	'field' => 'website',
	'icon' => 'globe',
	'type' => 'group',
	'fields' => $fields,
	'helpText' => 'Add the physical address of this place / business',
	'model' => $model
])