<?php
$tags = (isset($place->tagList) ? $place->tagList : '');
// dd($place->tagList);
?>

@include('templates.partials.input', [
	'id'	=> 'tags',
	'field' => 'tags',
	'icon' => 'tag',
	'placeholder'=>'Enter tags...', 
	'helpText' => 'Separate tags with commas. <br> <small>(3 tags max)</small>',
	'model' => $place,
	'value' => $tags
])
