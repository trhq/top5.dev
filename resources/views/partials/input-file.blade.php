<?php 
if ( ! isset($value) ) $value = null; 
?>
<div class="form-group row
	@if ( $errors->first($field) ) 
		has-error
	@endif
">
	<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
		<p class="input-group">
				{{-- Output a file field --}}
				{!! Form::file($field, $value, [
					"class" => "form-control",
					"placeholder" => $placeholder
				]) !!}
		</p>

		@if ( $errors->first($field) ) 
			<p class="alert alert-danger col-xs-12"> 
				<span class="glyphicon glyphicon-exclamation-sign"></span>
				{!! $errors->first($field) !!} 
			</p>
		@endif
	</div>
	@if(isset($helpText))
		<p class="help-block col-sm-6 col-md-5 col-lg-4">
			{{$helpText}}
		</p>
	@endif
</div>
