<?php 
	$helpText = ( isset($helpText) ? $helpText : 'Describe your place <br> <small>(255 characters max)</small>'); 
?>

@include('templates.partials.input', [
	'field' => 'description',
	'icon' => 'globe',
	'type' => 'textarea',
	'placeholder'=>'Enter a description...', 
	'helpText' => $helpText,
	'model' => $model
])