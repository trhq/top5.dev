@if( ! $invoice->date_paid && ! Auth::user()->is_admin)
    {{ Form::open([
		'url' => route('user.invoices.update', ['user' => $user->slug,'invoice' => $invoice->slug]),
		'method' => 'put'
	])}}
@if(Request::url() !== route('user.invoices.show', ['user' => $user->slug,'invoice' => $invoice->slug]))
	<button type="submit" class="btn btn-success col-xs-5 col-sm-5 col-xs-push-2" title="Pay {{$invoice->invoiceNumber}}">
		<span class="glyphicon glyphicon-usd"></span>
		<span class="hidden-xs hidden-sm">
			Pay
		</span>
	</button>
@else
<br>
	<button type="submit" class="btn btn-success col-xs-12" title="Pay {{$invoice->invoiceNumber}}">
		<span class="glyphicon glyphicon-usd"></span>
		<span class="">
			Pay Now
		</span>
	</button>
@endif

	{{ Form::close() }}
@endif