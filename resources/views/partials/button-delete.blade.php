{{-- Delete Button triggers Modal --}}

<button type="button" class="btn btn-danger col-sm-4" data-toggle="modal" data-target="#trash{{$place->slug}}">
  	<span class="glyphicon glyphicon-trash"></span>
	<span class="hidden-xs hidden-sm hidden-md">
		Trash
	</span>
</button>

{{-- Modal dialog presents warning to user --}}
<div class="modal fade" id="trash{{$place->slug}}" tabindex="-1" role="dialog" aria-labelledby="trash{{$place->slug}}Label">
  <div class="modal-dialog" role="document">
    <article class="modal-content">
      <header class="modal-header bg-danger text-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="trash{{$place->slug}}Label">
        	<span class="glyphicon glyphicon-exclamation-sign"></span> Warning!
        </h3>
      </header>
      <section class="modal-body">
      	<p class="lead">You are about to send <strong class="text-danger">{{$place->name}}</strong> to the trash.</p>
      	<p class="lead">Are you sure you want to proceed?</p> 
      </section>
      <footer class="modal-footer bg-danger text-danger">
        {{ Form::open([
			'url' => route('user.places.destroy', ['user' => $place->user->slug, 'place' => $place->slug]),
			'method' => 'delete'
		])}}
{{-- button cancels deletion --}}
        <button type="button" class="btn btn-default" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span>
				<span class="hidden-xs hidden-sm">Cancel</span>
        </button>
{{-- button proceeds with deletion --}}
		<button type="submit" class="btn btn-danger ">
			<span class="glyphicon glyphicon-trash"></span>
			<span class="hidden-xs hidden-sm">
				Trash
			</span>
		</button>
		{{ Form::close() }}
      </footer>
    </article>
  </div>
</div>
