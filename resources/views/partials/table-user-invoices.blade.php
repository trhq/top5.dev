<article class="panel panel-primary">
	<header class="panel-heading container-fluid">
		<h4 class="col-xs-6">Your Invoices</h4>
		<p class="col-xs-6 text-right"> 
			@include('partials.input-results_per_page')
		</p>
	</header>

@if($invoices->count())
	<table class="table table-striped">
	<thead>
		<tr>
			<th class="col-sm-2">Invoice#</th>
			<th class="col-sm-1 hidden-xs hidden-sm text-center">Views</th>
			<th class="col-sm-1 hidden-xs hidden-sm text-center">Rate</th>
			<th class="col-sm-2 text-center">Total</th>
			<th class="col-sm-3 hidden-xs text-center">Due</th>
			<th class="col-xs-4 col-sm-4 col-md-3 text-center">Actions</th>
		</tr>
	</thead>
	@foreach($invoices as $invoice) 
		<tr>
			<th> {{$invoice->invoiceNumber}} </th>
			<td class="hidden-xs hidden-sm text-center"> 
				{{$invoice->display_total}}
			</td>
			<td class="hidden-xs hidden-sm text-center"> 
				{{$invoice->invoiceRate}}
			</td>
			<td class="text-center">
				{{$invoice->invoiceTotal}}
			</td>
			<td class="hidden-xs text-center">
				@include('partials.invoice-due')
			</td>
			<td class="container text-center">
				<a href="{{route('user.invoices.show', ['user' => $invoice->user->slug,'invoice' => $invoice->slug])}}" 
				   class="btn btn-default 
					@if( $invoice->date_paid || Auth::user()->is_admin)
				   		col-xs-12
				   	@else 
				   		col-xs-5
					@endif
				    "
				   title="View {{$invoice->invoiceNumber}}">
					<span class="glyphicon glyphicon-search"></span>
					<span class="hidden-xs hidden-sm">
						View
					</span>
				</a>
				@include('partials.invoice-pay')
			</td>
		</tr>
	@endforeach
	</table>
	</ul>
@else 
	<div class="panel-body">
		<p class="lead"> You have no invoices yet. </p>
	</div>
@endif
@if ($invoices->links())
	<footer class="text-center panel-footer">
		{{$invoices->links()}}
	</footer>
@endif
</article>