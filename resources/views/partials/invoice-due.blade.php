{{-- Determine whether to display overdue date, due date or date paid. --}}
@if( ! $invoice->date_paid)
	@if ($invoice->overdue)
		<span class="text-danger"> <strong>Overdue: {{$invoice->dueDate}}</strong> </span>
	@else
		Due: {{$invoice->dueDate}}
	@endif
@else 
	<span class="text-success">
		Paid: {{$invoice->date_paid->format('d-M-Y')}}
	</span>
@endif