<?php
if (isset($model)) {
	$modelName = str_replace('top5\\', '', get_class($model));
	switch ($modelName) {
		case 'Place': 
			$helpText = "Please enter an email to use when contacting this place.";
			break;
		case 'User':
			$helpText = "Enter a unique email address.";
			break;
		default:
			$helpText ='';
	}
} else {
	$model = null;
	$helpText = 'Enter your email';
}
?>
@include('templates.partials.input', [
	'field' => 'email',
	'icon' => 'envelope',
	'type' => 'email',
	'placeholder'=>'Enter an email...', 
	'helpText' => $helpText,
	'model' => $model
])