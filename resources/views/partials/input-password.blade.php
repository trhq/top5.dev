	@if( ! isset($confirm) )
		@include('templates.partials.input', [
			'field' => 'password',
			'type' => 'password',
			'icon' => 'lock',
			'placeholder'=>'Enter a password...', 
			'helpText' => 'Enter a password. <br> <small>(6 characters min)',
			'model' => $model
		])
	@else
		@include('templates.partials.input', [
			'type' => 'group',
			'field' => 'password',
			'icon' => 'lock',
			'placeholder'=>'Enter a password...', 
			'helpText' => 'Enter a password. <br> <small>(6 characters min)',
			'model' => $model,
			'fields' => array(
				Form::password('password', array(
					"class" => "form-control",
					"placeholder" => "Password",
				)),
				Form::password("password_confirmation", array(
					"class" => "form-control",
					"placeholder" => "Confirm",
				)),
			)
		])
	@endif
