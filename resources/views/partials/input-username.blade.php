
@include('templates.partials.input', [
	'field' => 'username',
	'icon' => '&#64;',
	'type' => 'text',
	'placeholder'=>'User Name', 
	'helpText' => 'Enter a unique user name. <br> <small>(12 characters max)</small>',
	'model' => $model
])
