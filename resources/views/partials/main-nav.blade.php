<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<!-- Brand and dropdown -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Top 5</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		
			<ul class="nav navbar-nav navbar-left">
				<li><a href="/#/places">Places</a></li>
				<li><a href="/#/tags">Tags</a></li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				@if(Auth::check())
					<li>
						<a href="{{ url('logout') }}" >Logout</a>
					</li>
				@else
					<li>
						<a title="Register with Top 5" href="{{ route('register') }}" >Register</a>
					</li>
					<li>
						<a title="Login to Top 5" href="{{ route('login') }}" >Login</a>
					</li>
				@endif
				<li class="dropdown">
				@if(Auth::check())				
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
						&#64;{{Auth::user()->username}}<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
							@if(Auth::user()->is_admin)
								<li>
									<a title="Administrative Area" href="{!!route('admin.index')!!}"><strong>Admin Panel</strong></a>
								</li>
								<li>
									<a title="Manage Users" href="{!!route('user.index')!!}">Users</a>
								</li>
								<li>
									<a title="Manage Places" href="{!!route('admin.places')!!}">Places</a>
								</li>
								<li>
									<a title="Manage Pricing" href="{!!route('admin.pricing')!!}">Manage Pricing</a>
								</li>
								<li>
									<a title="Manage Billing" href="{!!route('admin.billing.index')!!}">Billing</a>
								</li>
							@endif
							<li>
								<a href="{{ route('user.show', ['slug' => \Auth::user()->slug]) }}" ><strong>Your Profile</strong></a>
							</li>
							@if( ! Auth::user()->is_admin)
								<li>
									<a href="{{ route('user.places.index', ['slug' => \Auth::user()->slug]) }}" >Your Places</a>
								</li>
								<li>
									<a href="{{ route('user.invoices.index', ['slug' => \Auth::user()->slug]) }}" >Your Bills</a>
								</li>
							@endif
					</ul>
				@endif
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->

</nav>

