@extends('templates.main')

@section('pageTitle', "Administration - Top 5")
@section('title', "Administration <small>Top 5</small>")

@section('content')


<section class="container">
<ul class="list-unstyled">
	<li>
		<a href="{!! route('user.index') !!}"
		class="btn btn-lg btn-block btn-default col-xs-12">
			<span class="glyphicon glyphicon-user"></span>
			Manage Users
		</a> 
	</li>

	<li>
		<a href="{!! route('user.index') !!}"
		class="btn btn-lg btn-block btn-default col-xs-12">
			<span class="glyphicon glyphicon-home"></span>
			Manage Places
		</a> 
	</li>

	<li>
		<a href="{!! route('admin.pricing') !!}" 
		class="btn btn-lg btn-block btn-default col-xs-12">
			<span class="glyphicon glyphicon-usd"></span>
			Manage Pricing
		</a> 
	</li>

	<li>
		<a href="{!! route('admin.billing.index') !!}" 
		class="btn btn-lg btn-block btn-default col-xs-12">
			<span class="glyphicon glyphicon-list-alt"></span>
			Manage Billing
		</a> 
	</li>
</ul>

</section>


@endsection