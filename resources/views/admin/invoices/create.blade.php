@extends('templates.main')

@section('pageTitle', "Billing Run - Top 5 Admin")
@section('title', "Billing Run <small>Top 5 Admin</small>")

@section('content')


<section class="container">
@if( ! $users->sum('BillingDisplays'))
	<h4>Not Enough Billable Displays</h4>
	<p class="lead">Currently, there are no users who have reached the minimum threshold for billing.</p>
@else
<h4>Forecast</h4>
<p>This is an estimate of the current billing period if you were to run billing now. Only users who have reach them minimum threshold of {{$pricing->min('minimum_displays')}} displays will be billed in this statement run. </p>

<table class="table table-responsive table-striped">
	<thead>
		<tr>
			<th>User</th>
			<th class="text-center">Clicks</th>
			<th class="text-center">Displays</th>
			<th class="text-right">Per Unit</th>
			<th class="text-right">Total</th>
		</tr>
	</thead>
	<tbody>
	@foreach($users as $user) 
		@if($user->lastDisplays > $pricing->min('minimum_displays'))
		<tr>
			<th> <strong>{!!$user->name!!}</strong></th>
			<td class="text-center"> {!! $user->lastClicks !!} </td>
			<td class="text-center"> {!! $user->lastDisplays !!} </td>
			@foreach($pricing as $price)
				@if($user->lastDisplays > $price->minimum)
					<td class="text-right">${{$price->price}}</td>
					<td class="text-right">${!! number_format($price->price * $user->lastDisplays,2) !!}</td>
					@break
				@endif
			@endforeach
		</tr>
		@endif
	@endforeach
	</tbody>
</table>

{!! Form::open(['method' => 'POST', 'url' => route('admin.billing.store')]) !!}
	{!!Form::hidden('run_billing', true)!!}
	<button type="submit" class="btn btn-block btn-success"> Run Billing Now </a>
{!! Form::close() !!}
@endif 

<p class="lead"><a href="{{route('admin.billing.index')}}" title="Return to Billing">Return to Billing</a>

</section>
<section>
<h4>Pricing Table</h4>
<ul>
	@foreach($pricing as $price)
		<li>
			Min Clicks: {{$price->minimum}} <br>
			Price: ${{$price->price}}
	@endforeach
</ul>
</section>

@endsection