@extends('templates.main')

@section('pageTitle', "Invoices - Top 5 Admin")
@section('title', "Invoices <small>Top 5 Admin</small>")

@section('content')
<article>
<section class="container">

<a href="{!! route('admin.billing.create') !!}" class="btn btn-default col-sm-4">
	<span class="glyphicon glyphicon-usd"></span>
	Do Billing Run
</a>
</section>
<section class="container row">
	<h3>Past Statements</h3>
	<table class="table table-responsive table-striped">
		<thead>
			<th> Run Number </th>
			<th> Date Run </th>
			<th> Invoices </th>
			<th> Total </th>
		</thead>
		<tbody>
		@foreach($statements as $statement) 
		<tr>
			<td>
				{{$statement->id}}
			</td>
			<td>
				{{$statement->run_at->format('d-M-Y')}}
			</td>
			<td>
				{{$statement->invoiceCount}}
			</td>
			<td>
				{{$statement->runTotal}}
			</td>
			<td>
				@include('templates.partials.button', array(
					'link'  => route('admin.billing.show', $statement->slug),
					'class' => 'btn btn-block btn-default',
					'icon'  => 'search',
					'title' => "View Statement #{$statement->id}",
					'text'  => "View"
				))
			</td>
		</tr>
		@endforeach
		</tbody>
	</table>
	</section>

	<hr> 
</article>
@endsection