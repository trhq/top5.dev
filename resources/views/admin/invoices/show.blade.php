@extends('templates.main')

@section('pageTitle', "$statement->statementNumber - Top 5 Admin")
@section('title', "$statement->statementNumber <small>Top 5 Admin</small>")

@section('content')
<article>
<section class="container row">

	<p class="lead text-left col-xs-4"> 
		<strong class="">Date Run:</strong>
		<span class="">{{$statement->run_at->format('d-M-Y')}}</span>
	</p>
	<p class="lead text-center col-xs-4"> 
		<strong class="">Invoices:</strong>
		<span class="">{{$statement->invoiceCount}}</span>
	</p>
	<p class="lead text-right col-xs-4"> 
		<strong class="">Total:</strong>
		<span class="">{{$statement->runTotal}}</span>
	</p>
				
</section>
	@include('partials.table-user-invoices', ['invoices' => $statement->invoices])
<footer>
	<a class="btn btn-lg btn-block btn-primary" href="{{route('admin.billing.index')}}" title="Back to Billing">Back to Billing</a>
</footer>	
</article>
@endsection