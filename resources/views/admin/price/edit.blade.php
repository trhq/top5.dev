@extends('templates.partials.form')

@section('formPageTitle', "Edit Pricing")

@section('formTitle', "Edit Pricing")

@section('form')

	{!! Form::model($prices, array(
		'method' => 'PUT',
		'url' => route('admin.pricing'),
		'class' => 'form container'
	)) !!}
	@foreach($prices as $price)
	<div class="row form-group">
			{!! Form::label(
				$price->minimum,
				'Over ' . $price->minimum, 
				['class' => 'col-xs-12']
			) !!}
			<div class="col-xs-10">
			{!! Form::text(
				"prices[$price->id]", $price->price, [
					'class' => 'form-control'
				]
			)!!}
			</div>
	</div>
	@endforeach
	<div class="row form-group">
		<div class="col-xs-10">
		{!! Form::button(
			'<span class="glyphicon glyphicon-ok"></span> Update Prices', [
			'class' => 'btn btn-primary btn-block col-xs-10',
			'type' => 'submit'
		])!!}
		{!! Form::close() !!}
		</div>
	</div>


@endsection