@extends('templates.main')

@section('pageTitle', "Places - Top 5")
@section('title', "Places <small>Top 5</small>")

@section('content')


<section class="container">
@include('partials.table-user-places')
</section>


@endsection