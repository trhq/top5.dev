@extends('templates.partials.form')

@section('formTitle', 'Login')

@section('form')

	{!! Form::open(array(
		'url' => 'login',
		'class' => 'form'
	)) !!}
	@include('partials.input-email', ['model' => new top5\User])
	@include('partials.input-password', ['model' => new top5\User])
	@include('partials.input-remember')
	<p class="form-group col-xs-12">
		{!! Form::submit('Login', ['class' => 'btn btn-primary col-xs-12'])!!}
	</p>
	{!! Form::close() !!}

@endsection
