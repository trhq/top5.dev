@extends('templates.partials.form')

@section('formPageTitle', 'User Registration')

@section('formTitle', 'Register')

@section('form')
	<?php $user = new top5\User; ?>
	{!! Form::open(array(
		'url' => 'register',
		'class' => 'form'
	)) !!}

	@include('user.form-user', ["model" => $user])

	<fieldset>
		@include('partials.input-password', ['model' => $user, 'confirm' => true])
	</fieldset>

	{!! Form::submit('Create my account', ['class' => 'btn btn-block btn-primary'])!!}

	{!! Form::close() !!}
@endsection
