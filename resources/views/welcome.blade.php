@extends('templates.main')

@section('pageTitle', 'Home')
@section('title', 'Welcome to Top 5')

@section('content')
                <div class="title">Top 5 Development!</div>
                <p>
                    @if(Auth::check())
                        Welcome back, {{ Auth::user()->name }}
                        <a href="{{ url('logout') }}" >Logout</a>
                    @else
                        Howdy Stranger
                        <a href="{{ url('login') }}" >Login</a> or
                        <a href="{{ url('register') }}" >Register</a> 

                    @endif
@endsection