<!DOCTYPE html>
<html ng-app="top5App">
	<head>
	 	<!-- Bootstrap -->
	 	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">



	    <script data-main="/js/config" src="/js/dist/require.js"></script>
	    <!-- Bootstrap -->
	    <link href="/css/dist/jquery-ui.min.css" rel="stylesheet">
	    <link href="/css/app.css" rel="stylesheet">

		<!-- Angular -->
		<script src="/js/components/lodash/dist/lodash.min.js"></script>
		<script src="/js/angular.min.js"></script>
		<script src="/js/angular-ui-router.min.js"></script>
		<script src="/js/angular-simple-logger.min.js"></script>
		<script src="/js/angular-google-maps.min.js"></script>

		<script src="https://maps.googleapis.com/maps/api/js?signed_in=true&key=AIzaSyDyX3eBfVbgu_W5SOfn-wPeYD8j1cYOO4s&language=en"></script>

		<!-- Site -->
		<title>Top 5</title>
		<script src="/app/top5.js"></script>
		<script src="/app/top5.directives.js"></script>
		<script src="/app/top5.controller.js"></script>
		<link rel="stylesheet" href="./css/style.css">
	</head>
	<body ng-controller="top5Controller as top5">
		<main ui-view>

		</main>
		@include('partials.message-flash')
		@include('partials.main-nav')
		{{-- @include('scripts.mapFrontEndDisplay') --}}
	</body>
</html>
