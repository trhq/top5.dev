{{-- Delete Button triggers Modal --}}

<?php
$class = (isset($class) ? $class : 'col-md-4 col-sm-4') . ' btn-danger btn';
?>

{{-- Modal dialog presents warning to user --}}
<div class="modal fade" id="trash{{$model->slug}}" tabindex="-1" role="dialog" aria-labelledby="trash{{$model->slug}}Label">
  <div class="modal-dialog" role="document">
    <article class="modal-content">
      <header class="modal-header bg-danger text-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="trash{{$model->slug}}Label">
        	<span class="glyphicon glyphicon-exclamation-sign"></span> Warning!
        </h3>
      </header>
      <section class="modal-body">
      	<p class="lead text-danger">You are about to send <strong class="text-danger">{!!$model->name!!}</strong> to the trash.</p>
      	@if($warning) 
        <p class="lead">{!!$warning!!}</p>
        @endif
        <p class="lead text-danger">Are you sure you want to proceed?</p> 
      </section>
      <footer class="modal-footer bg-danger text-danger">
        {{ Form::open([
          'url' => $route,
          'method' => 'delete'
		    ])}}
  {{-- button cancels deletion --}}
        <button type="button" class="btn btn-default" data-dismiss="modal">
				<span class="glyphicon glyphicon-remove"></span>
				<span class="hidden-xs hidden-sm">Cancel</span>
        </button>
  {{-- button proceeds with deletion --}}
		<button type="submit" class="btn btn-danger ">
			<span class="glyphicon glyphicon-trash"></span>
			<span class="hidden-xs hidden-sm">
				Trash
			</span>
		</button>
		{{ Form::close() }}
      </footer>
    </article>
  </div>
</div>

@include('templates.partials.button', array(
  'class' => $class,
  'btn'   => 'true',
  'type'  => 'submit',
  'icon'  => 'trash',
  'title' => "Delete $model->name",
  'text'  => 'Trash',
  'attr'  => array(
    "data-toggle" => "modal",
    "data-target" => "#trash$model->slug",
  )
))