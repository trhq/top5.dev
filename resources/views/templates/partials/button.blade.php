<?php
$class = (isset($class) ? 'btn ' . $class : ' btn-default btn col-md-4 col-sm-4') ;
$attr = (isset($attr) ? $attr : []);
$attributes = '';
foreach($attr as $attribute => $value) {
  $attributes.= " $attribute=$value ";
}
?>
@if(isset($btn))
<button type="button" title="{{$title}}" class="{{$class}}" {{$attributes}}>
    <span class="glyphicon glyphicon-{{$icon}}"></span>
  <span class="hidden-xs hidden-sm">
    {{$text}}
  </span>
</button>

@else
  <a href="{{$link}}" class="{{$class}}" title="{{$title}}">
    <span class="glyphicon glyphicon-{{$icon}}"></span>
    <span class="hidden-xs hidden-sm">
      {{$text}}
    </span>
  </a>
@endif