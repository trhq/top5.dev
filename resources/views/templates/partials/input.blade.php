<?php 
if ( ! isset($value) ) $value = null; 
if ( ! isset($type) ) $type = 'text';
?>

@if($type !== 'group')
<?php
$options = array(
	"id"	=> $field,
	"placeholder" => $placeholder,
	"class" => "form-control",
);
?>
@endif
<div class="form-group row
	@if ( $errors->first($field) ) 
		has-error
	@endif
">
	<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
		<p class="input-group">
			<span class="input-group-addon" id="basic-addon1">
			@if(strpos($icon, '&#') === 0)
				{{$icon}}			
			@else
				<span class="glyphicon glyphicon-{{$icon}}"></span>
			@endif
			</span>
			@if($type === 'text')
				{{-- Output a text field --}}
				{!! Form::text($field, $value, $options) !!}
			@elseif($type === 'password')
				{{-- Output a text field --}}
				{!! Form::password($field, $options) !!}
			@elseif($type === 'email')
				{{-- Output an email field --}}
				{!! Form::email($field, $value, $options) !!}
			@elseif($type === 'url')
				{{-- Output a url field --}}
				{!! Form::url($field, $value, $options) !!}
			@elseif($type === 'file')
				{{-- Output a file field --}}
				{!! Form::file($field, $value, $options) !!}
			@elseif($type === 'textarea')
				{{-- Output a textarea --}}
				{!! Form::textarea($field, $value, $options) !!}
			@elseif($type === 'group')
				{{-- Output each field in the array --}}
				@for ($i=0; $i < count($fields); $i++)
    				{{$fields[$i]}}
				@endfor
			@endif
		</p>
		@if ( $errors->first($field) ) 
			<p class="alert alert-danger col-xs-12"> 
				<span class="glyphicon glyphicon-exclamation-sign"></span>
				{!! $errors->first($field) !!} 
			</p>
		@endif
	</div>
	@if(isset($helpText))
		<p class="help-block col-sm-6 col-md-5 col-lg-4 form-helper">
			{!!$helpText!!}
		</p>
	@endif
</div>
