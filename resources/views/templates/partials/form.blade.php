@extends('templates.main')
@section('pageTitle')
	@yield('formPageTitle')
@endsection


@section('title')
	@yield('formTitle')
@endSection

@section('content')
	@yield('form')
@endsection

@section('javascript')
	@include('scripts.formhelpers')
	@include('scripts.mapCoordinates')
@endsection