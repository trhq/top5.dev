<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script data-main="/js/config" src="/js/dist/require.js"></script>
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/dist/jquery-ui.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>@yield('pageTitle') - Top5</title>

  </head>
<body class="container">
<header>
  <h1>@yield('title')</h1>
</header>
<main>

@include('partials.message-flash')

@yield('content')


</main>

@yield('javascript')

@include('partials.main-nav')
<footer>
<p>Page Footer Goes Here</p>
</footer>
</body>
</html>
