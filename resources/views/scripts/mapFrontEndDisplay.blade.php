<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script type="text/javascript">
console.log('Map Script');
$(document).ready(function() {
console.log('Doc Ready');
/**
 * If there is a #map element, load google maps API.
 */
if (document.querySelectorAll('#map').length > 0)
	{
		console.log('Map Found');
		var apiKey = 'AIzaSyDyX3eBfVbgu_W5SOfn-wPeYD8j1cYOO4s',
			js_file = document.createElement('script');
		    js_file.type = 'text/javascript';
		    js_file.src = 'https://maps.googleapis.com/maps/api/js?callback=initMap&signed_in=true&key='+apiKey+'&language=en';
		$('head').append(js_file);
	} else {
		console.log('No Map Found');
	}
}); // End document.ready

/**
 * Place address and coordinate fields.
 * @type {jQuery}
 */
var $locationInputs = $('.coords');
/**
 * Refresh the map when the address has been changed.
 */
var timer = null;
$locationInputs.change(function(){
	console.log(coords);
	timer = setTimeout(initMap, 500);
})

/**
 * Adds a height to the google maps element 
 * if there are coordinates given.
 */
function showMap() {
	if ( $('input[name=lat]').val() || 
		 $('input[name=lng]').val() ) {
		console.log('adding map');
		$('#map').height(400);
	}
}

/**
 * Get the inputted coordinate values.
 * @return {array}
 */
function coords() {
	var coords = [{
		lat : $('input[name=lat]').val(),
		lng : $('input[name=lng]').val()
	}];
	return coords;
}

/**
 * Initialise the map 
 */
var map;
function initMap()
{
	showMap();
	map = new google.maps.Map($('#map')[0], {
		center: {lat: 0, lng: 0},
		zoom: 1
	});
	plotMarkers(coords());
}

/**
 * Re-centers the map when the window is resized.
 */
var resizeTimer = null;
$(window).bind('resize' , function(){
	resizeTimer = setTimeout(centerMap, 1000);
});

/**
 * Centers the map on the map's coordinates.
 */
function centerMap() {
	coords = {
		lat : $('input[name=lat]').val(),
		lng : $('input[name=lng]').val()
	};
	var position = new google.maps.LatLng(coords.lat, coords.lng);
	map.setCenter(position);
}

/**
 * Plots the map markers on the map.
 */
var markers;
var bounds;
function plotMarkers(m)
{
  	markers = [{lat: $('input[name=lat]').val(), lng: $('input[name=lng]').val()}];
  	bounds = new google.maps.LatLngBounds();

  	m.forEach(function (marker) {
    var position = new google.maps.LatLng(marker.lat, marker.lng);

    markers.push(
		new google.maps.Marker({
	        position: position,
	        map: map,
	        animation: google.maps.Animation.DROP,
		})
	);
    bounds.extend(position);
	map.setCenter(position);
	map.setZoom(18);
  });
}
</script>

