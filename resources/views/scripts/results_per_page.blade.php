<script>
require(['jquery'], function($) {
	$(document).ready(function(){
		$('select[name=results_per_page]')
			.bind('change', function(){
				var results_per_page = $(this).find('option:selected').text();
				var request = window.location;
				console.log(request.search);
				request.search = '?results_per_page=' + results_per_page;
			})
	}); // document.ready
}); // Require
</script>