<script type="text/javascript">
//lat = -37.817369
//lng = 144.954585
require(['jquery'], function($) {
	$(document).ready(function(){
		var apiKey = 'AIzaSyDyX3eBfVbgu_W5SOfn-wPeYD8j1cYOO4s';
		/**
		 * Placeholder object for holding coordinates.
		 * @type {Object}
		 */
		var coords = {};

		/**
		 * The Url for making a geocode request.
		 * @type {String}
		 */
		var requestUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

		/**
		 * The form fields containing the address data.
		 * @type {jQuery}
		 */
		var $address  = $('input[name=address]'),
			$suburb   = $('input[name=suburb]'),
			$postcode = $('input[name=postcode]'),
			$state    = $('select[name=state]');
		
		/**
		 * Sets the address coordinates if they are contained
		 * within the response data.
		 * @param {JSON} data A google maps geocode response.
		 */
		function setAddressCoords(data) {
			if (data.status == 'OK') {
				coords = data.results[0].geometry.location;
				$('input[name=lat]').val(coords.lat);
				$('input[name=lng]').val(coords.lng);
			}
		}
		
		/**
		 * Sends a request for geodata for the supplied address
		 * @param  {string} address  The street address
		 * @param  {string} suburb   The address suburb
		 * @param  {string} postcode The address postcode
		 * @param  {string} state    The request state
		 * @return {void}       
		 */
		function requestAddressCoords(address, suburb, postcode, state) {
			// Format address strings for request
			var regex = new RegExp(' ', 'g');
			address = address.replace(regex, '+');
			suburb = suburb.replace(regex, '+');
			// Request geocode from Google Maps
			$.getJSON(
				requestUrl+address+','+suburb+',+'+state,
				function(data) { setAddressCoords(data); }
			);
		}

		/**
		 * Checks the address form fields and if the address is
		 * complete, makes a request to the google maps API.
		 * @return {void}
		 */
		function getAddressCoords() {
			var address  = $address.val(),
				suburb   = $suburb.val(),
				postcode = $postcode.val(),
				state    = $state.val();

			if (address && suburb && postcode && state) {
				requestAddressCoords(address, suburb, postcode, state);
			}
		}

		/**
		 * Binds getAddressCoords to changes in the form fields.
		 */
		$($address).bind('change', getAddressCoords);
		$($suburb).bind('change', getAddressCoords);
		$($postcode).bind('change', getAddressCoords);
		$($state).bind('change', getAddressCoords);
	
	}); // document.ready
}); // Require
</script>