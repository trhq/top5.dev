<script>
require(['jquery'], function($) {
	$(document).ready(function(){
		var $elements = $('.form-group'),
			$fields = $elements.find('input, textarea, select'),
			timer = null;
		
		// Add handlers for showing, hiding and updating helper
		// classes to display on input forms. 
		$elements.each(function(){
			var $formGroup = $(this),
				$fields = $formGroup.find('input, textarea, select');
			
			// Shows the helper text if it's not already visible.
			$fields.bind('focus', function(){
				var $this = $(this), 
					$group = $this.closest('.form-group'), 
					$help = $group.find('.help-block');
				if ( ! $help.is(':visible'))
					$help.slideDown(250);
			});	

			// Update contextual class for a required, non-empty field.
			$fields.bind('keydown', function(){
				var $this = $(this), 
					$help = $this.closest('.form-group').find('.help-block');
				if( $help.find('span.text-danger') && $this.val() != '' ) {
					$help.find('span')
						 .removeClass('text-danger')
						 .addClass('text-success');
				}
				if( $help.find('span.text-success') && $this.val() == '' ) {
					$help.find('span')
						 .removeClass('text-success')
						 .addClass('text-danger');
				}
			});

			// Hides the helper text after user leaves the field
			// if no other fields in the group is focused.
			$fields.bind('focusout', function(){
				var $this = $(this),
					$group = $this.closest('.form-group');
				// Wait 1/5 sec and then check for group focus
				setTimeout(function(){
					if ($group.find(":focus").length == 0) {
						// If no elements are in the current field group, 
						// hide the helper text after 1/20 sec.
						timer = setTimeout(function(){
							$group = $this.closest('.form-group');
							$group.find('.help-block').slideUp(250);
						}, 50);
	    			}
				} ,200)
			});

		});

	}); // document.ready
}); // Require
</script>