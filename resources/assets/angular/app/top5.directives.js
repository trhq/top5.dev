/**
 * A place listing with partial information and link to full information.
 */
app.directive('placeListing', function(){
	return {
		templateUrl: '../templates/partials/place-listing.html'
	};
});
app.directive('topMap', function(){
	return {
		templateUrl: '../templates/partials/top-map.html'
	};
});
/**
 * A list of the current view's terms with click events
 * to set each as the currently displayed term.
 */
app.directive('termList', function(){
	return {
		templateUrl: '../templates/partials/term-list.html'
	};
});
