var top5;
var app = angular.module('top5App', ['ui.router', 'uiGmapgoogle-maps']);
	/* App configuration */

app.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	/* Home Page */
	$stateProvider.state('home', {
		url: '/',
		templateUrl: '../templates/home.html',
		controller: 'top5Controller as top5'
	});
	/* Places index. */
	$stateProvider.state('places', {
		url: '/places',
		templateUrl: '../templates/places.html',
		controller: 'top5Controller as top5'
	});
	/* Individual place. */
	$stateProvider.state('places-single', {
		url: '/places/:slug',
		templateUrl: '../templates/places-single.html',
		controller: 'top5Controller as top5'
	});
	/* Tags index. */
	$stateProvider.state('tags', {
		url: '/tags',
		templateUrl: '../templates/tags.html',
		controller: 'top5Controller as top5'
	});
	/* Individual tag. */
	$stateProvider.state('tags-single', {
		url: '/tags/:slug',
		templateUrl: '../templates/tags-single.html',
		controller: 'top5Controller as top5'
	});
	/* User Index. */
	$stateProvider.state('user', {
		url: '/user',
		templateUrl: '../templates/user.html',
		controller: 'top5Controller as top5'
	});
	/* Individual User. */
	$stateProvider.state('user-single', {
		url: '/user/:slug',
		templateUrl: '../templates/user-single.html',
		controller: 'top5Controller as top5'
	});
});
