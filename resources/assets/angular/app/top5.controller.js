/* App control */

app.controller('top5Controller', function($http, $state) {
	/**
	 * Controller Properties
	 */
	top5              = this;	// Self referential/
	top5.state        = $state; // Application state.

	top5.displayTerm  = null; 	// The currently displayed term {tag, user, suburb ... }.
	top5.displayPlace = null; 	// The currently displayed place.
	top5.hideTermLink = false;	// Hides the link to single term view.
	top5.places       = [];		// Currently visible places.

	top5.mapEl        = document.querySelector('#map');
	top5.mapApi		  = "AIzaSyDyX3eBfVbgu_W5SOfn-wPeYD8j1cYOO4s";

	top5.pages        = [];		// Pagination Control.
	top5.terms        = [];		// All current results, sorted by term {tag, user, suburb ... }.
	/**
	 * Enables Angular UI google-map directives.
	 */
	angular.extend(top5, {
	    map: {
	            center: {
	                latitude: 42.3349940452867,
	                longitude:-71.0353168884369
	            },
	            zoom: 14,
	            markers: [],
	            events: {
	            load: function (map, eventName, originalEventArgs) {
	                top5.map.markers = top5.markers;
	                top5.map.markers.push(marker);
	                top5.$apply();
	            }
	    	}
	    }
	});
	/**
	 * Sets the map markers for the list of currently displayed places.
	 * @return {void}
	 */
	top5.setMarkers = function() {
		top5.map.markers = [];
		top5.places.forEach(function(place){
			top5.map.markers.push({
			    id: place.slug,
			    title: place.name + ' - ' + place.suburb,
			    coords: {
	 				"latitude":place.lat,
					"longitude":place.lng
			    },
			    click: function() {
			    	top5.setPlace(place.slug);
			    }
			});
		});
		top5.map.setBounds();
	};
	/**
	 * Calculates the northeastern and
	 * southeasternmost coordinates.
	 * @return {json}
	 */
	top5.map.setBounds = function() {
		var min = { latitude: null, longitude: null};
		var max = { latitude: null, longitude: null};
		/* Check each marker to find min and max*/
		top5.map.markers.forEach(function(marker){
			if ( ! min.latitude  ) min.latitude = max.latitude = marker.coords.latitude;
			if ( ! min.longitude ) min.longitude = max.longitude = marker.coords.longitude;

			if ( marker.coords.latitude < min.latitude ) min.latitude = marker.coords.latitude;
			if ( marker.coords.longitude < min.longitude ) min.longitude = marker.coords.longitude;

			if ( marker.coords.latitude > max.latitude ) max.latitude = marker.coords.latitude;
			if ( marker.coords.longitude > max.longitude ) max.longitude = marker.coords.longitude;
		});

		top5.map.bounds = { southwest: min, northeast: max };
	};
	/**
	 * Searches the map markers for id matching
	 * slug and returns its coordinates.
	 * @param  {string} slug   The place slug.
	 * @return {json} coords   Google map coordinates.
	 */
	top5.getMarker = function(slug) {
		var ii = 0, ll = top5.map.markers.length;
		for(ii; ii < ll; ii++) {
			if(top5.map.markers[ii].id == slug) {
				return {
					latitude:  top5.map.markers[ii].coords.latitude,
					longitude: top5.map.markers[ii].coords.longitude,
				};
			}
		}
	};
	/**
	 * Gets the places for the currently displayed term.
	 * @return {void}
	 */
	top5.setPlaces = function() {
		var ii = 0,
			ll = top5.terms.length;
		for(ii; ii < ll; ii++) {
			if(top5.terms[ii].slug == top5.displayTerm) {
				top5.places = top5.terms[ii].places;
				break;
			}
		}
		top5.setMarkers();
	};
	/**
	 * Sets, switches and unsets the current place.
	 * @param {string} slug
	 * @return {void}
	 */
	top5.setPlace = function(slug) {
		if(top5.displayPlace != slug) {
			top5.displayPlace = slug;
			top5.map.center = top5.getMarker(slug);
			top5.map.zoom = 19	;
		} else {
			top5.displayPlace = null;
			top5.map.setBounds();
		}
	};
	/**
	 * Sets the term to display and then gets its places.
	 * @param  {string} name 	The name of the term to display.
	 * @return {void}
	 */
	top5.setTerm = function(slug) {
		if (top5.displayTerm != slug) {
			top5.displayTerm = slug;
			top5.setPlaces();
		}
	};
/**
 * Request Handling
 */
	/**
	 * Determines, sets and returns the API request Url.
	 * @return {string} Url
	 */
	top5.setRequestUrl = function() {
		top5.requestUrl = '/api' + top5.state.current.url.replace(':slug', top5.state.params.slug);
		top5.hideTermLink = (top5.state.params.slug ? true : false);
		return top5.requestUrl;
	};
	/**
	 * Parses api response and sets terms and places.
	 * @param  {$http.response} response
	 * @return {void}
	 */
	top5.processResponse = function(response) {
		// Save response to terms list.
		top5.terms = response.data;
		top5.setTerm(top5.terms[0].slug);

		// Add pagination controls, if there are any.
		 top5.pages = top5.terms.pages;
		 return;
	};
	/**
	 * Process a request, if there is a valid request url.
	 */
	top5.setRequestUrl();
	if(top5.requestUrl.indexOf('^') < 1)
	{
		$http.get(top5.requestUrl).then(top5.processResponse);
	}
});
