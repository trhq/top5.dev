//var elixir = require('laravel-elixir');

var gulp = require('gulp'),
	less = require('gulp-less'),
	plumber = require('gulp-plumber'),
	prefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify');

var paths = {
	jsDist : './public/js/dist',
	cssDist : './public/css',
	fontDist : './public/fonts',
	bower : './bower_components/'
};

gulp.task('buildjs', function(){
	gulp.src(paths.bower + 'jquery/dist/jquery.min.js')
		.pipe(gulp.dest(paths.jsDist));
	gulp.src(paths.bower + 'requirejs/require.js')
		.pipe(uglify())
		.pipe(gulp.dest(paths.jsDist));
	gulp.src(paths.bower + '/bootstrap/dist/js/bootstrap.min.js')
		.pipe(gulp.dest(paths.jsDist));
});

// Compiles LESS > CSS 
gulp.task('bootstrap', function(){
	gulp.src(paths.bower + 'bootstrap/dist/fonts/**')
		.pipe(gulp.dest(paths.fontDist));
    return gulp.src('bootstrap.less')
        .pipe(less())
        .pipe(gulp.dest(paths.cssDist));
});


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

//elixir(function(mix) {
//    mix.sass('app.scss');
//});
